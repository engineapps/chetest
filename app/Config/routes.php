<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'communities', 'action' => 'index', 'residential'));
	Router::connect('/admin', array('controller' => 'pages', 'action' => 'home', 'admin' => true));


	Router::connect('/zoomify/*', array('controller' => 'pages', 'action' => 'zoomify'));
	Router::connect('/thumbs/*', array('controller' => 'pages', 'action' => 'thumbs'));
	Router::connect('/page/contact', array('controller' => 'pages', 'action' => 'contact'));
	Router::connect('/page/*', array('controller' => 'pages', 'action' => 'index'));
	Router::connect('/communities/view/*', array('controller' => 'communities', 'action' => 'view'));
	Router::connect('/communities/*', array('controller' => 'communities', 'action' => 'index'));

	Router::redirect('/owa/*', 'http://173.239.168.122/owa');

	// casl routes
	Router::connect('/casl/*', array('controller' => 'subscriptions', 'action' => 'index'));
	Router::connect('/casl-consent/*', array('controller' => 'subscriptions', 'action' => 'consent'));
	Router::connect('/casl-no-consent/*', array('controller' => 'subscriptions', 'action' => 'no_consent'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
