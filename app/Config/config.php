<?php
/**
 * This is site configuration file.
 *
 * Use it to configure website specific settings of Cake.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 2.0.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
$config = array(
	'Database' => array(
		'default' => array(
			'datasource' => 'Database/Mysql',
			'persistent' => false,
			'host' => 'localhost',
			'login' => 'voghomes_main',
			'password' => 'HOpaM5^2Fbn6',
			'database' => 'voghomes_main',
			'prefix' => ''
		)
	),
	'App' => array(
		'baseUrl' => '',
		'name' => 'Vogue Development Group',
		'email' => 'info@voguehomes.ca',
		'homecenter' => 'models@voguehomes.ca',
		'emailNotify' => 'abeandfrances@gmail.com',
		'phone' => '(905) 886-8500',
		'fax' => '(111) 111-1111',
		'Address' => array(
			'company' => 'Vogue Group',
			'address' => '1118 Centre Street',
			'city' => 'Thornhill',
			'province' => 'Ontario',
			'postal' => 'L4J 7R9',
			'geo' => array(
				'lat' => '43.778748',
				'lng' => '-79.569749'
			)
		),
		'Facebook' => array(
			'appId' => '',
			'appSecret' => '',
			'url' => 'https://www.facebook.com/VogueDevelopmentGroup',
		),
		'Twitter' => array(
			'username' => '',
			'url' => 'https://twitter.com/',
			'oauth_access_token' => "",
			'oauth_access_token_secret' => "",
			'consumer_key' => "",
			'consumer_secret' => ""
		),
		'GoogleAnalytics' => array(
			'viewId' => '87476506', // Where to find this? Navigate to Google Analytics => Admin => Select {XYZ} Account => VIEW (PROFILE) => View Settings => View ID (usually 8 digits)
			'webPropertyId' => 'UA-52033304-1',
			'webDomainName' => 'voguegroup.com',
			'gapiEmail' => 'kris.karski@gmail.com',
			'gapiPassword' => 'uoyorauuyvahamhy'
		)
	),
    'Email' => array(
        'transport' => 'Smtp',
        'host' => 'smtp.sendgrid.net',
        'port' => 587,
        'timeout' => 5,
        'username' => 'engineapps',
        'password' => 'engine@advertising',
        'from' => 'info@voguehomes.ca'
    ),
	'Recaptcha' => array(
		'disable' => false,
		'apiServer' => 'http://www.google.com/recaptcha/api',
		'apiSecureServer' => 'https://www.google.com/recaptcha/api',
		'verifyServer' => 'www.google.com',
		'pubKey' => '6Lez58cSAAAAAG8Ap9nvSjLIQD-_A9MD3LoGTI-L',
		'privateKey' => '6Lez58cSAAAAAH6viMNzuyDPS791MtxhXVSxk7Uq'
	),
	'Cache' => array(
		'disable' => false,
		'check' => true
	),
	'debug' => 0,
	'MinifyAsset' => true
);