<?php
App::uses('AppHelper', 'View/Helper');

/**
 * Cakephp view helper to interface with http://code.google.com/p/minify/ project.
 * Minify: Combines, minifies, and caches JavaScript and CSS files on demand to speed up page loads.
 * Requirements:
 * 	An entry in core.php - "MinifyAsset" - value of which is either set 'true' or 'false'.
 * 	False would be usually set during development and/or debugging. True should be set in production mode.
 *
 * @package		app.View.Helper
 */
class MinifyHelper extends AppHelper {

	public $helpers = array('Html');

	/**
	 * Returns one or many `<script>` tags depending on the number of scripts given.
	 */
	public function script($url, $revision = '0') {
		if (Configure::read('MinifyAsset') === true) {
			return '<script type="text/javascript" src="' . Router::url($this->_path($url, 'js', $revision)) . '"></script>';
		} else {
			return $this->Html->script($url);
		}
	}

	/**
	 * Creates a link element for CSS stylesheets.
	 */
	public function css($path, $revision = '0') {
		if (Configure::read('MinifyAsset') === true) {
			return '<link rel="stylesheet" type="text/css" href="' . Router::url($this->_path($path, 'css', $revision)) . '" />';
		} else {
			return $this->Html->css($path);
		}
	}

	/**
	 * Build path for minified files
	 *
	 * @param mixed $assets
	 * @param string $ext
	 * @param string $revision
	 * @return string
	 */
	private function _path($assets, $ext, $revision = '0') {
		if (!is_array($assets)) {
			$assets = array($assets);
		}

		$path = '/min-' . $ext . '?f=';
		$path .= implode(',', $assets);
		$path .= '&r=' . $revision;

		return $path;
	}

}
