$(function(){
	$('textarea[class!=noEditor]').redactor({
		imageGetJson: WEBROOT + 'admin/pages/images',
		imageUpload: WEBROOT + 'admin/pages/upload'
	});
});