$(function() {
	app.init();
});

var app = (function () {
	var articleTop = $('#main-wrapper').offset();

	var init = function () {
		set_bindings();
		$('.fancybox').fancybox();

		$('[data-toggle="map"]').each(function(){
			$this = $(this);
			mapNow($this.attr('id'), $this.attr('data-lat'), $this.attr('data-lng'), $this.attr('data-content'));
		});

		$('.js-masonry img').load(function(){
			$('.js-masonry').masonry();
		});

		$('.js-masonry').masonry({
			itemSelector: '.item'
		});
	};
	
	var set_bindings = function () {
		//$(window).bind('scroll', nav_glue);
	};
	
	var nav_glue = function () {
		console.log(articleTop);
		if ($(window).scrollTop() > 242) {
			$('.navbar').addClass('glued');
		} else if ($(window).scrollTop() <= 242) {
			$('.navbar').removeClass('glued');
		}
	}
	
	var mapNow = function (element_id, lat, lng, title) {
		var myLatlng = new google.maps.LatLng(lat, lng);
		var mapOptions = {
			zoom: 12,
			scrollwheel: false,
			disableDefaultUI: false,
			center: myLatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById(element_id), mapOptions);
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title: '',
		});
		var infowindow = new google.maps.InfoWindow({
			content: title
		});
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map, marker);
		});
	}

	return {
		init: init
	};
})();