<div class="content panel panel-default col-md-10">
	<h3 class="panel-heading"><?php echo __('Offices'); ?></h3>
	<table class="table table-striped">
	<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th><?php echo $this->Paginator->sort('hours'); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($offices as $office): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $office['Office']['id']; ?>&nbsp;</td>
		<td><?php echo $office['Office']['name']; ?>&nbsp;</td>
		<td><?php echo $office['Office']['hours']; ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('1'), array('action' => 'moveup', $office['Office']['id'], 1), array('class' => 'btn btn-primary')); ?>
			<?php echo $this->Html->link(__('1'), array('action' => 'movedown', $office['Office']['id'], 1), array('class' => 'btn btn-primary')); ?>
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $office['Office']['id']), array('class' => 'btn btn-primary')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $office['Office']['id']), array('class' => 'btn btn-primary')); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $office['Office']['id']), array('class' => 'btn btn-primary'), __('Are you sure you want to delete # %s?', $office['Office']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="col-md-2 panel-default aside">
	<h3 class="panel-heading"><?php echo __('Actions'); ?></h3>
	<ul class="list-unstyled">
		<li><?php echo $this->Html->link(__('New Office'), array('action' => 'add'), array('class' => 'btn btn-primary')); ?></li>
	</ul>
</div>