<div class="content panel panel-default col-md-10">
	<?php echo $this->Form->create('Office', array('type' => 'file')); ?>
	<h3 class="panel-heading"><?php echo __('Admin Edit Office'); ?></h3>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('hours');
		echo $this->Form->input('lat');
		echo $this->Form->input('lng');
		echo $this->Form->input('description');
		echo $this->Form->input('map', array('type' => 'file', 'after' => $this->Html->image('/thumbs/img/offices/100____0____' . $this->request->data['Office']['map'])));
		echo $this->Form->input('is_active');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="col-md-2 panel-default aside">
	<h3 class="panel-heading"><?php echo __('Actions'); ?></h3>
	<ul class="list-unstyled">
		<li><?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $this->Form->value('Office.id')), array('class' => 'btn btn-primary'), __('Are you sure you want to delete # %s?', $this->Form->value('Office.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Offices'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?></li>
	</ul>
</div>