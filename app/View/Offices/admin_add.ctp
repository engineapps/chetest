<div class="content panel panel-default col-md-10">
	<?php echo $this->Form->create('Office', array('type' => 'file')); ?>
	<h3 class="panel-heading"><?php echo __('Admin Add Office'); ?></h3>
	<fieldset>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('hours');
		echo $this->Form->input('lat');
		echo $this->Form->input('lng');
		echo $this->Form->input('description');
		echo $this->Form->input('map', array('type' => 'file'));
		echo $this->Form->input('is_active');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="col-md-2 panel-default aside">
	<h3 class="panel-heading"><?php echo __('Actions'); ?></h3>
	<ul class="list-unstyled">
		<li><?php echo $this->Html->link(__('List Offices'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?></li>
	</ul>
</div>