<div class="content">
	<h1><?php echo __('Link'); ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $link['Link']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $link['Link']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Url'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $link['Link']['url']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Link'), array('action' => 'edit', $link['Link']['id']), array('class' => 'edit')); ?></li>
		<li><?php echo $this->Html->link(__('Delete Link'), array('action' => 'delete', $link['Link']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $link['Link']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('List Links'), array('action' => 'index', $link['Link']['menu_id']), array('class' => 'list')); ?></li>
		<li><?php echo $this->Html->link(__('New Link'), array('action' => 'add', $link['Link']['menu_id']), array('class' => 'add')); ?></li>
	</ul>
</div>