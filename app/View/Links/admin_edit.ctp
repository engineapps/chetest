<div class="content">
	<h1><?php echo __('Admin Edit Link'); ?></h1>
	<?php echo $this->Form->create('Link', array('type' => 'file')); ?>
	<fieldset>
		<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('subtext');
		echo $this->Form->input('url');
		echo $this->Form->input('menu_id');
		echo $this->Form->input('parent_id', array('options' => $links, 'empty' => __('-- Top Level --')));
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Links'), array('action' => 'index', $this->request->data['Link']['menu_id']), array('class' => 'list')); ?></li>
	</ul>
</div>
<script>
	$("#LinkMenuId").change(function() {
		window.location = "<?php echo $this->Html->url(array('action' => 'edit', $this->request->data['Link']['id'])); ?>" + "/" + $(this).val();
	});
</script>