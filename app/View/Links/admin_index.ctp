<div class="content">
	<h1><?php echo __('Links'); ?></h1>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo __('Status'); ?></th>
			<th><?php echo $this->Paginator->sort('Menu.name', __('Menu')); ?></th>
			<th class="text-right"><?php echo __('Actions'); ?></th>
		</tr>
		<?php $i = 0; ?>
		<?php foreach ($links as $link): ?>
			<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
			<tr class="<?php echo $class; ?>">
				<td><?php echo $link['Link']['id']; ?>&nbsp;</td>
				<td>
					<?php echo $link['Link']['name']; ?><br />
					<?php echo $this->Html->link($link['Link']['url'], $link['Link']['url'], array('class' => 'links')); ?>
				</td>
				<td>&nbsp;</td>
				<td><?php echo $link['Menu']['name']; ?>&nbsp;</td>
				<td class="text-right">
					<?php echo $this->Html->link(__('1'), array('action' => 'moveup', $link['Link']['id'], '1'), array('class' => 'up')); ?>
					<?php echo $this->Html->link(__('1'), array('action' => 'movedown', $link['Link']['id'], '1'), array('class' => 'down')); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $link['Link']['id']), array('class' => 'edit')); ?>
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $link['Link']['id']), array('class' => 'view')); ?>
					<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $link['Link']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $link['Link']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Link'), array('action' => 'add', $menu_id), array('class' => 'add')); ?></li>
	</ul>
</div>
<script>
	setTimeout(checkLinks, 10000);
	
	function checkLinks() {
		$('a.links').each(function(i, link) {
			if($(link).attr('href').match(/^javascript/i)) {
				$(link).parent().next().html('<div class="flashMessage notice">Not a link</div>');
			} else if($(link).attr('href').match(/^http:\/\//i)) {
				$(link).parent().next().html('<div class="flashMessage notice">External link</div>');
			} else {
				$.ajax({
					url: $(link).attr('href'),
					success: function() {
						$(link).parent().next().html('<div class="flashMessage success">Valid link</div>');
					},
					error: function() {
						$(link).parent().next().html('<div class="flashMessage message">Invalid link</div>');
					}
				});
			}
		});
	}
</script>