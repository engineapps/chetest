<!doctype html>
<!--[if IE 8]>    <html class="ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
	<head>
		<title><?php echo $title_for_layout; ?></title>
		<link href="<?php echo $this->Html->url('/favicon.ico?v=1'); ?>" rel="shortcut icon" />
		<?php
		echo $this->Html->charset();
		echo !empty($keywords_for_layout) ? $this->Html->meta(array('name' => 'keywords', 'content' => $keywords_for_layout)) : "";
		echo !empty($description_for_layout) ? $this->Html->meta(array('name' => 'description', 'content' => $description_for_layout)) : "";
		echo $this->Html->meta(array('name' => 'author', 'content' => 'Engine Advertising Inc'));
		echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0'));
		?>
		<script>
			var WEBROOT = "<?php echo Router::url('/'); ?>";
		</script>
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
		<?php echo $this->Minify->css(array('styles', '../js/libs/fancybox/jquery.fancybox'), '100'); ?>
		<?php echo $this->Minify->script(array('libs/jquery', 'libs/fancybox/jquery.fancybox', 'libs/masonry.pkgd', 'libs/bootstrap', 'app'), '100'); ?>
		<!--[if lt IE 9]>
			<?php echo $this->Minify->script(array('libs/html5shiv', 'libs/respond.min'), '100'); ?>
		<![endif]-->
		<script type="text/javascript" src="//use.typekit.net/lpa2waw.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	</head>
	<body>
		<header class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo $this->Html->url('/'); ?>"><?php echo $this->Html->image('logo.jpg'); ?></a>
				</div>
				<div class="navbar-collapse collapse main-menu">
					<ul class="nav navbar-nav navbar-right">
						<?php echo $nav['markup']['main-menu']; ?>
					</ul>
				</div>
			</div>
		</header>
		<article id="main-wrapper">
			<div class="container">
				<?php echo $content_for_layout; ?>
			</div>
		</article>
		<footer>
			<div class="container">
				<h3>Contact Us</h3>
				<div class="row">
					<div class="contact-slip">
						<ul class="list-unstyled">
							<li><?php echo $appConfig['Address']['address']; ?></li>
							<li><?php echo $appConfig['Address']['city']; ?>, <?php echo $appConfig['Address']['province']; ?></li>
							<li><?php echo $appConfig['Address']['postal']; ?></li>
						</ul>
					</div>
					<div class="contact-slip">
						<ul class="list-unstyled">
							<li><?php echo $appConfig['phone']; ?></li>
							<li><a href="mailto:<?php echo $appConfig['email']; ?>" rel="nofollow"><?php echo $appConfig['email']; ?></a></li>
							<li><a href="https://goo.gl/maps/Bs7V5" rel="nofollow" target="_blank">Map Directions <span class="glyphicon glyphicon-chevron-right"></span></a></li>
						</ul>
					</div>
					<div class="legal">
						<hr class="visible-xs">
						<ul class="list-inline">
							<li><a href="<?php echo $this->Html->url(array('controller' => 'page', 'action' => 'privacy-policy')); ?>"><?php echo __('Privacy Policy'); ?></a></li>
							<li><a href="<?php echo $this->Html->url(array('controller' => 'page', 'action' => 'terms-of-use')); ?>"><?php echo __('Terms of Use'); ?></a></li>
						</ul>
						<?php echo __('&copy; %s. Vogue Development Group.<br>All Rights Reserved.', date('Y')); ?>
					</div>
				</div>
			</div>
		</footer>
		<?php echo $this->element('tracking'); ?>
		<!--[if lt IE 7]>
			<script>
				$("body").html("<p style=\"padding: 10%; text-align: center;\">Your browser is <em>ancient!</em> <a href=\"http://browsehappy.com/\">Upgrade to a different browser</a> or <a href=\"http://www.google.com/chromeframe/?redirect=true\">install Google Chrome Frame</a> to experience this site.</p>");
			</script>
		<![endif]-->
	</body>
</html>