<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

?>
<!doctype html>
<!--[if lt IE 7]> <html lang="en-us" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en-us" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en-us" class="no-js ie8"> <![endif]-->
<!--[if IE 9]>    <html lang="en-us" class="no-js ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-us" class="no-js"> <!--<![endif]-->
	<head>
		<?php echo $this->Html->charset(); ?> 
		<title><?php echo __('Admin') ?> :: <?php echo $title_for_layout; ?></title>
		<?php echo $this->Html->meta('icon'); ?> 
		<?php echo $this->Html->meta(array('name' => 'author', 'content' => 'Engine Advertising Inc')); ?> 
		<?php echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width')); ?> 
		<link rel="apple-touch-icon" href="<?php echo $this->Html->url('/img/apple-touch-icon.png'); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<script>
			var WEBROOT = "<?php echo Router::url('/'); ?>";
		</script>
		<link href='//fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>
		<?php Configure::write('MinifyAsset', false); ?>
		<?php echo $this->Html->css(array('admin', '../js/redactor/redactor')); ?> 
		<?php echo $this->Html->script(array('libs/jquery.min', 'libs/modernizr.min', 'redactor/redactor', 'plugins', 'admin')); ?> 
	</head>
	<body>
		<section class="container">
			<?php if($this->Session->read('Auth.User.is_admin') || $this->Session->read('Auth.User.is_special')): ?>
			<header>
				<section class="contents clearfix">
					<a class="logo left" href="<?php echo $this->Html->url(array("controller" => "pages", "action" => "home", "admin" => true)); ?>">
						<?php echo $this->Html->image('engine_logo.png'); ?>
					</a>
					<div class="right" style="line-height: 2em;">
						<?php echo __('Welcome %s', $this->Session->read('Auth.User.first_name')) ?>, <?php echo $this->Html->link(__('[Logout]'), array('controller' => 'users', 'action' => 'logout')); ?><br />
						<?php echo $this->Html->link(__('[Website Frontend]'), '/'); ?>
					</div>
				</section>
				<?php if($this->Session->read('Auth.User.is_admin')): ?>
				<nav class="clearfix">
					<ul>
						<?php echo $nav['markup']['admin-menu']; ?>
					</ul>
				</nav>
				<?php endif; ?>
				<br class="clear" />
			</header>
			<section class="contents clearfix">
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->element('breadcrumbs'); ?>
				<?php echo $content_for_layout; ?>
				<div class="actions">
					<h1><?php echo __('Search Panel') ?></h1>
					<?php echo $this->Form->create('Search', array('url' => array('controller' => 'pages', 'action' => 'home'), 'type' => 'get')); ?>
					<?php echo $this->Form->input('Search.q', array('label' => false, 'div' => false, 'placeholder' => __('Search Here ...'), 'value' => @$q)); ?>
					<?php echo $this->Form->end(); ?>
				</div>
			</section>
			<footer class="clearfix">
				<?php echo $this->Html->link(__('Clear All Cache'), array('controller' => 'pages', 'action' => 'purge')); ?> | <?php echo __('Script by <a href="%s">%s</a>', 'http://www.engineadvertising.ca/', 'Engine Advertising Inc'); ?>
			</footer>
			<script>
				$(function() {
					$('table tr td[contenteditable]').on('click', function() {
						$(this).addClass('editable');
					}).on('blur', function() {
						$(this).removeClass('editable');
						$(this).addClass('blink');
						var id = $(this).parent().find('td:first').text();
						var url = '<?php echo $this->Html->url(array('action' => 'edit')); ?>/' + id;
						var index = $(this).parent().children().index($(this));
						var heading = $(this).closest("table").find("th").eq(index).find('a:first').attr('href');
						if (typeof heading == undefined || heading == null) {
							heading = '';
						}
						var matches = heading.match(/\/sort:([a-z_0-9]{1,})\//i);

						if (matches != null && typeof matches[1] != undefined) {
							var data = 'data[id]=' + id + '&data[' + matches[1] + ']=' + encodeURIComponent($(this).text());
							var item = $(this);
							$.ajax({
								url: url,
								data: data,
								type: 'POST',
								success: function(response) {
									item.removeClass('blink');
								}
							});
						}
					});
				});
			</script>
			<?php else: ?>
				<?php echo $content_for_layout; ?>
			<?php endif; ?>
		</section>
		<?php echo $this->element('sql_dump'); ?>
	</body>
</html>