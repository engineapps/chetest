<div class="content">
	<h1><?php echo __('Attachments'); ?></h1>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo __('Attachment'); ?></th>
			<th><?php echo __('Home'); ?></th>
			<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($attachments as $attachment): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $attachment['Attachment']['id']; ?>&nbsp;</td>
		<td><?php echo $attachment['Attachment']['name']; ?>&nbsp;</td>
		<td><?php echo $attachment['Home']['name']; ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $attachment['Attachment']['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $attachment['Attachment']['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $attachment['Attachment']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $attachment['Attachment']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Attachment'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>