<div class="content">
	<h1><?php echo __('Attachment'); ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $attachment['Attachment']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $attachment['Attachment']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Uploaded'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($attachment['Attachment']['uploaded']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Home'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Html->link($attachment['Home']['name'], array('controller' => 'homes', 'action' => 'view', $attachment['Home']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Attachment'), array('action' => 'edit', $attachment['Attachment']['id']), array('class' => 'edit')); ?></li>
		<li><?php echo $this->Html->link(__('Delete Attachment'), array('action' => 'delete', $attachment['Attachment']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $attachment['Attachment']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('List Attachment'), array('action' => 'index', $attachment['Home']['id']), array('class' => 'list')); ?></li>
		<li><?php echo $this->Html->link(__('New Attachment'), array('action' => 'add', $attachment['Home']['id']), array('class' => 'add')); ?></li>
	</ul>
</div>