<div class="content">
	<?php echo $this->Form->create('Attachment', array('type' => 'file')); ?>
	<h1><?php echo __('Admin Add Attachment'); ?></h1>
	<fieldset>
	<?php
		echo $this->Form->input('name', array('type' => 'file'));
		echo $this->Form->input('home_id');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Attachments'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>