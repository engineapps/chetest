<div class="content">
	<h1><?php echo __('Admin Edit Attachment'); ?></h1>
	<?php echo $this->Form->create('Attachment', array('type' => 'file')); ?>
	<fieldset>
		<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('type' => 'file', 'after' => $this->Html->link('/files/attachments/' . $this->request->data['Attachment']['name'])));
		echo $this->Form->input('home_id');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Attachments'), array('action' => 'index', $this->request->data['Attachment']['home_id']), array('class' => 'list')); ?></li>
	</ul>
</div>