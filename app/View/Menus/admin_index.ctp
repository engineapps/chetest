<div class="content">
	<h1><?php echo __('Menus'); ?></h1>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('slug'); ?></th>
			<th class="text-right"><?php echo __('Actions'); ?></th>
		</tr>
		<?php $i = 0; ?>
		<?php foreach ($menus as $menu): ?>
			<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
			<tr class="<?php echo $class; ?>">
				<td><?php echo $menu['Menu']['id']; ?>&nbsp;</td>
				<td><?php echo $menu['Menu']['name']; ?>&nbsp;</td>
				<td><?php echo $menu['Menu']['slug']; ?>&nbsp;</td>
				<td class="text-right">
					<?php echo $this->Html->link(__('Links'), array('controller' => 'links', 'action' => 'index', $menu['Menu']['id']), array('class' => 'link')); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $menu['Menu']['id']), array('class' => 'edit')); ?>
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $menu['Menu']['id']), array('class' => 'view')); ?>
					<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $menu['Menu']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $menu['Menu']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Menu'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>