<div class="content">
	<h1><?php echo __('Menu'); ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $menu['Menu']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $menu['Menu']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Slug'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $menu['Menu']['slug']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Menu'), array('action' => 'edit', $menu['Menu']['id']), array('class' => 'edit')); ?></li>
		<li><?php echo $this->Html->link(__('Delete Menu'), array('action' => 'delete', $menu['Menu']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $menu['Menu']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('List Menus'), array('action' => 'index'), array('class' => 'list')); ?></li>
		<li><?php echo $this->Html->link(__('New Menu'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>
<div class="related">
	<h1><?php echo __('Related Links'); ?></h1>
	<?php if (!empty($menu['Link'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('url'); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($menu['Link'] as $link): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
		<tr class="<?php echo $class; ?>">
			<td><?php echo $link['id']; ?></td>
			<td><?php echo $link['name']; ?></td>
			<td><?php echo $link['url']; ?></td>
			<td class="text-right">
				<?php echo $this->Html->link(__('View'), array('controller' => 'links', 'action' => 'view', $link['id']), array('class' => 'view')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'links', 'action' => 'edit', $link['id']), array('class' => 'edit')); ?>
				<?php echo $this->Html->link(__('Delete'), array('controller' => 'links', 'action' => 'delete', $link['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $link['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php endif; ?>
</div>