<div class="content">
	<h1><?php echo __('Admin Add Menu'); ?></h1>
	<?php echo $this->Form->create('Menu', array('type' => 'file')); ?>
	<fieldset>
		<?php
		echo $this->Form->input('name');
		echo $this->Form->input('slug');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Menus'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>