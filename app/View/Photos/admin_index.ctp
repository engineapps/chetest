<div class="content">
	<h1><?php echo __('Photos'); ?></h1>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo __('Photo'); ?></th>
		<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th><?php echo $this->Paginator->sort('Community.name', __('Community')); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($photos as $photo): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $photo['Photo']['id']; ?>&nbsp;</td>
		<td><img src="<?php echo $this->Html->url('/thumbs/img/communities/100____0____'.$photo['Photo']['name']); ?>" /></td>
		<td><?php echo $photo['Photo']['name']; ?>&nbsp;</td>
		<td><?php echo $this->Html->link($photo['Community']['name'], array('controller' => 'communities', 'action' => 'view', $photo['Community']['id'])); ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('1'), array('action' => 'moveup', $photo['Photo']['id'], '1'), array('class' => 'up')); ?>
			<?php echo $this->Html->link(__('1'), array('action' => 'movedown', $photo['Photo']['id'], '1'), array('class' => 'down')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $photo['Photo']['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $photo['Photo']['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $photo['Photo']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $photo['Photo']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Photo'), array('action' => 'add', $community_id), array('class' => 'add')); ?></li>
	</ul>
</div>