<div class="content">
	<h1><?php echo __('Admin Add Photo'); ?></h1>
	<?php echo $this->Form->create('Photo', array('type' => 'file')); ?>
	<fieldset>
		<?php
		echo $this->Form->input('title');
		echo $this->Form->input('name', array('type' => 'file'));
		echo $this->Form->input('community_id');
		echo $this->Form->input('type');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Photos'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>