<div class="content">
	<h1><?php echo __('Admin Add Image'); ?></h1>
	<?php echo $this->Form->create('Image', array('type' => 'file')); ?>
	<fieldset>
		<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('type' => 'file', 'after' => $this->Html->image('/thumbs/img/homes/100____0____' . $this->request->data['Image']['name'])));
		echo $this->Form->input('name_alternative', array('type' => 'file', 'after' => $this->Html->image('/thumbs/img/homes/100____0____' . $this->request->data['Image']['name_alternative'])));
		echo $this->Form->input('price');
		echo $this->Form->input('size');
		echo $this->Form->input('seo_title');
		echo $this->Form->input('seo_keywords');
		echo $this->Form->input('seo_description');
		echo $this->Form->input('elevation_id');
		echo $this->Form->input('home_id');
		echo $this->Form->input('is_sold');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Images'), array('action' => 'index', $this->request->data['Image']['home_id']), array('class' => 'list')); ?></li>
	</ul>
</div>