<div class="content">
	<h1><?php echo __('Images'); ?></h1>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo __('Image'); ?></th>
		<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th><?php echo $this->Paginator->sort('price'); ?></th>
		<th><?php echo $this->Paginator->sort('Elevation.name', __('Elevation')); ?></th>
		<th><?php echo $this->Paginator->sort('Home.name', __('Home')); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($images as $image): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $image['Image']['id']; ?>&nbsp;</td>
		<td><img src="<?php echo $this->Html->url('/thumbs/img/homes/100____0____'.$image['Image']['name']); ?>" /></td>
		<td><?php echo $image['Image']['name']; ?>&nbsp;</td>
		<td><?php echo $image['Image']['price']; ?>&nbsp;</td>
		<td><?php echo $this->Html->link($image['Elevation']['name'], array('controller' => 'elevations', 'action' => 'view', $image['Elevation']['id'])); ?>&nbsp;</td>
		<td><?php echo $this->Html->link($image['Home']['name'], array('controller' => 'homes', 'action' => 'view', $image['Home']['id'])); ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $image['Image']['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $image['Image']['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $image['Image']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $image['Image']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Image'), array('action' => 'add', $home_id), array('class' => 'add')); ?></li>
	</ul>
</div>