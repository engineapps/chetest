<?php

/**
 * Cakephp view helper to interface with http://code.google.com/p/minify/ project.
 * Minify: Combines, minifies, and caches JavaScript and CSS files on demand to speed up page loads.
 * @author: Ketan Shah - ketan.shah@gmail.com - http://www.innovatechnologies.in
 * Requirements: An entry in core.php - "minify" - value of which is either set 'true' or 'false'. False would be usually set during development and/or debugging. True should be set in production mode.
 */
Class MinifyHelper extends AppHelper {

	public $inlineAssets = array('js' => array(), 'css' => array());
	public $helpers = array('Html'); //used for seamless degradation when minify is set to false;

	public function js($assets, $revision = 1) {
		if (Configure::read('minify')) {
			echo sprintf("<script type='text/javascript' src='%s'></script>", $this->_path($assets, $revision, 'js'));
		} else {
			echo $this->Html->script($assets);
		}
	}

	public function css($assets, $revision = 1) {
		if (Configure::read('minify')) {
			echo sprintf("<link type='text/css' rel='stylesheet' href='%s' />", $this->_path($assets, $revision, 'css'));
		} else {
			echo $this->Html->css($assets);
		}
	}

	private function _path($assets, $revision, $ext) {
		if (!empty($this->inlineAssets[$ext])) {
			$assets = array_merge($assets, $this->inlineAssets[$ext]);
			$this->inlineAssets[$ext] = array();
		}
		$path = $this->webroot . "min/?f=";
		foreach ($assets as $asset) {
			$path .= $this->webroot . $ext . "/" . ( $asset . ",");
		}
		$path = substr($path, 0, count($path) - 2);
		$path .= "&revision=" . $revision;
		return $path;
	}

}