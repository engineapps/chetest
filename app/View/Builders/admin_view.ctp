<div class="content">
	<h1><?php echo __('Builder'); ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $builder['Builder']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $builder['Builder']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Image'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $builder['Builder']['image']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $builder['Builder']['description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($builder['Builder']['created']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($builder['Builder']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Builder'), array('action' => 'edit', $builder['Builder']['id']), array('class' => 'edit')); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Builder'), array('action' => 'delete', $builder['Builder']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $builder['Builder']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Builders'), array('action' => 'index'), array('class' => 'list')); ?> </li>
		<li><?php echo $this->Html->link(__('New Builder'), array('action' => 'add'), array('class' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h1><?php echo __('Related Homes'); ?></h1>
	<?php if (!empty($builder['Home'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($builder['Home'] as $home): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
		<tr class="<?php echo $class; ?>">
			<td><?php echo $home['id']; ?></td>
			<td><?php echo $home['name']; ?></td>
			<td><?php echo $this->Time->niceShort($home['modified']); ?></td>
			<td class="text-right">
				<?php echo $this->Html->link(__('View'), array('controller' => 'homes', 'action' => 'view', $home['id']), array('class' => 'view')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'homes', 'action' => 'edit', $home['id']), array('class' => 'edit')); ?>
				<?php echo $this->Html->link(__('Delete'), array('controller' => 'homes', 'action' => 'delete', $home['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $home['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php endif; ?>
</div>