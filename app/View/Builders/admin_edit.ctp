<div class="content">
	<?php echo $this->Form->create('Builder', array('type' => 'file')); ?>
	<h1><?php echo __('Admin Edit Builder'); ?></h1>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('image', array('type' => 'file', 'after' => $this->Html->image('/thumbs/img/builders/100____0____'.$this->data['Builder']['image'])));
		echo $this->Form->input('description');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Builders'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>