<div class="content">
	<?php echo $this->Form->create('Builder', array('type' => 'file')); ?>
	<h1><?php echo __('Admin Add Builder'); ?></h1>
	<fieldset>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('image', array('type' => 'file'));
		echo $this->Form->input('description');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Builders'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>