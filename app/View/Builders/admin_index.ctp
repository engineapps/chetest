<div class="content">
	<h1><?php echo __('Builders'); ?></h1>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($builders as $builder): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $builder['Builder']['id']; ?>&nbsp;</td>
		<td><?php echo $builder['Builder']['name']; ?></td>
		<td><?php echo $this->Html->image('/thumbs/img/builders/100____0____' . $builder['Builder']['image']); ?></td>
		<td><?php echo $this->Time->niceShort($builder['Builder']['created']); ?>&nbsp;</td>
		<td><?php echo $this->Time->niceShort($builder['Builder']['modified']); ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $builder['Builder']['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $builder['Builder']['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $builder['Builder']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $builder['Builder']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Builder'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>