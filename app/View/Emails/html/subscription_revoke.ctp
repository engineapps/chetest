<p>
	<?php echo __('Hello'); ?>,<br />
	<br />
	<?php echo __('Please verify the withdrawal of your consent by clicking the link below. Please note that your withdrawal is not complete until the link below is clicked and a verification page is presented.'); ?><br /><br />
	<a href="<?php echo $this->Html->url(array('controller' => 'subscriptions', 'action' => 'subscribe', $data['Subscription']['hash'], '0'), true); ?>"><?php echo __('Click here to Revoke your Consent'); ?></a><br />
	<br />
	<?php echo __('Regards'); ?><br />
	<?php echo $data['Email']['fromName']; ?>
</p>