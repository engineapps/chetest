<p>
	<?php echo __('Hello'); ?>,<br />
	<br />
	<?php echo __('Thank you for updating your email address. To complete the process please click on the link below.'); ?><br /><br />
	<a href="<?php echo $this->Html->url(array('controller' => 'subscriptions', 'action' => 'subscribe', $data['Subscription']['hash'], '1'), true); ?>"><?php echo __('Click here to Confirm your Consent'); ?></a><br />
	<br />
	<?php echo __('Regards'); ?><br />
	<?php echo $data['Email']['fromName']; ?>
</p>