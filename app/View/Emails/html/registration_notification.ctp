<p><?php echo __('Dear Admin'); ?></p>

<p>
    <?php echo __('<b>%s %s</b> successfully registered with below communities:', $data['Registration']['first_name'], $data['Registration']['last_name']); ?><br />
    <?php if (!empty($data['Community']['Community'])): ?>
        <b><?php echo implode('<br />', $data['Data']['communities']); ?></b>
    <?php endif; ?>
</p>

<p>
    <?php echo __('Regards') ?><br />
    <?php echo $data['Data']['name']; ?>
</p>