<p><?php echo __('Dear %s', $data['Registration']['first_name']); ?></p>

<p>
    <?php echo __('You have successfully registered with %s for below communities:', $data['Data']['name']); ?><br />
    <?php if (!empty($data['Community']['Community'])): ?>
        <b><?php echo implode('<br />', $data['Data']['communities']); ?></b>
    <?php endif; ?>
</p>

<p>
    <?php echo __('Regards') ?><br />
    <?php echo $data['Data']['name']; ?>
</p>