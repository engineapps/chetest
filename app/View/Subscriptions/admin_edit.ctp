<div class="content panel panel-default col-md-10">
	<h3 class="panel-heading"><?php echo __('Admin Edit Subscription'); ?></h3>
	<?php echo $this->Form->create('Subscription'); ?>
	<fieldset>
		<?php
		echo $this->Form->input('id');
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('email');
		echo $this->Form->input('is_subscribed');
		?>
	</fieldset>
	<?php echo $this->Form->end(array('label' => __('Submit'), 'class' => 'btn btn-primary')); ?>
</div>
<div class="col-md-2 panel-default aside">
	<h3 class="panel-heading"><?php echo __('Actions'); ?></h3>
	<ul class="list-unstyled">
		<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-remove"></span> ' . __('Delete'), array('action' => 'delete', $this->Form->value('Subscription.id')), array('class' => 'btn btn-primary'), __('Are you sure you want to delete # %s?', $this->Form->value('Subscription.id'))); ?></li>
		<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-list"></span> ' . __('List Subscriptions'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?></li>
	</ul>
</div>