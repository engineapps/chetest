<section class="oneway clearfix">
	<div class="wrapper double">
		<div class="packed">
			<h1><?php echo __('I Consent to Receive Electronic Communications'); ?></h1>
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->Form->create('Subscription'); ?>
			<p><?php echo __('In accordance with Canadian Anti-Spam Legislation I give my consent to receive electronic communications from %s. including: bulletins, e-vites and other similar materials.', $appConfig['name']); ?></p>
			<?php echo $this->Form->input('email'); ?>
			<br />
			<p><?php echo __('I understand that I may withdraw my consent at anytime by visiting'); ?> <a href="<?php echo $this->Html->url('/casl', true); ?>"><?php echo $this->Html->url('/casl', true); ?></a></p>
			<div class="text-center">
				<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</section>