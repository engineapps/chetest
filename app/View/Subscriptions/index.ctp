<section class="oneway clearfix">
	<div class="wrapper double">
		<div class="packed">
			<h1><?php echo __('Consent Withdrawal'); ?></h1>
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->Form->create('Subscription', array('id' => 'StyledForm')); ?>
			<p><?php echo __('I would like to withdraw my consent and discontinue receiving commercial electronic messages from  %s.', $appConfig['name']); ?></p>
			<?php echo $this->Form->input('hash', array('type' => 'hidden')); ?>
			<?php echo $this->Form->input('email'); ?>
			<p><?php echo $this->Form->input('is_subscribed', array('label' => __('I agree and understand that in order to complete this process I will have to click on a verification link sent to the above noted email address, if the verification process is not completed then my consent to receive electronic communications will not be revoked and shall continue. If you require any assistance regarding this process you may contact us at <a href="mailto:%s">%s</a>', $appConfig['email'], $appConfig['email']))); ?></p>
			<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</section>