<section class="oneway clearfix">
	<div class="wrapper double">
		<div class="packed">
			<h1><?php echo __('Consent Withdrawal'); ?></h1>
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->Form->create('Subscription'); ?>
			<p><?php echo __('I would like to withdraw my consent and discontinue receiving commercial electronic messages from %s. If you require any assistance regarding this process you may contact us at <a href="mailto:%s">%s</a>', $appConfig['name'], $appConfig['email_casl'], $appConfig['email_casl']); ?></p>
			<?php echo $this->Form->input('email', array('disabled' => 'disabled')); ?>
			<br />
			<div class="text-center">
				<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</section>