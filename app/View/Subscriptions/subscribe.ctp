<section class="oneway clearfix">
	<div class="wrapper double">
		<div class="packed">
			<h1><?php echo __('Thank You'); ?></h1>
			<?php echo $this->Session->flash(); ?>
			<?php if ($subscription['Subscription']['is_subscribed']): ?>
				<?php echo __('You are Successfully Subscribed to receive Electronic Communications from %s.', $appConfig['name']); ?>
			<?php else: ?>
				<?php echo __('Your Consent is Successfully Withdrawn from %s.', $appConfig['name']); ?>
			<?php endif; ?>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		</div>
	</div>
</section>