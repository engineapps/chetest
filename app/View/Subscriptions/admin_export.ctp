<?php
$fp = fopen('php://output', 'w');
$emails = array();
$header = array('id', 'first_name', 'last_name', 'email', 'created');
foreach ($communities as $community) {
	$row = array(__('%s Subscriptions', $community['Community']['name']), '', '', '', '');
	fputcsv($fp, $row);
	fputcsv($fp, $header);
	foreach ($community['CommunitySubscription'] as $subscription) {
		if (!empty($subscription['is_subscribed']) && !empty($subscription['Subscription']['email'])) {
			$row = array(
				$subscription['Subscription']['id'],
				$subscription['Subscription']['first_name'],
				$subscription['Subscription']['last_name'],
				$subscription['Subscription']['email'],
				$subscription['Subscription']['created']
			);
			fputcsv($fp, $row);
		}
	}
}
$row = array(__('Generic Subscriptions'), '', '', '', '');
fputcsv($fp, $row);
fputcsv($fp, $header);
foreach ($subscriptions as $subscription) {
	if (!empty($subscription['Subscription']['is_subscribed'])) {
		$row = array(
			$subscription['Subscription']['id'],
			$subscription['Subscription']['first_name'],
			$subscription['Subscription']['last_name'],
			$subscription['Subscription']['email'],
			$subscription['Subscription']['created']
		);
		fputcsv($fp, $row);
	}
}
fclose($fp);