<div class="content panel panel-default col-md-10">
	<h3 class="panel-heading"><?php echo __('Admin Add Subscription'); ?></h3>
	<?php echo $this->Form->create('Subscription'); ?>
	<fieldset>
	<?php
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('email');
		echo $this->Form->input('is_subscribed', array('checked' => 'checked'));
	?>
	</fieldset>
	<?php echo $this->Form->end(array('label' => __('Submit'), 'class' => 'btn btn-primary')); ?>
</div>
<div class="col-md-2 panel-default aside">
	<h3 class="panel-heading"><?php echo __('Actions'); ?></h3>
	<ul class="list-unstyled">
		<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-list"></span> ' . __('List Subscriptions'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?></li>
	</ul>
</div>