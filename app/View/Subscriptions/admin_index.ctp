<div class="content panel panel-default col-md-10">
	<h3 class="panel-heading"><?php echo __('Subscriptions'); ?></h3>
	<div class="table-responsive">
		<table class="table table-striped">
			<tr>
				<th><?php echo $this->Paginator->sort('id'); ?></th>
				<th><?php echo $this->Paginator->sort('full_name'); ?></th>
				<th><?php echo $this->Paginator->sort('email'); ?></th>
				<th><?php echo $this->Paginator->sort('is_subscribed'); ?></th>
				<th class="text-right"><?php echo __('Actions'); ?></th>
			</tr>
			<?php $i = 0; ?>
			<?php foreach ($subscriptions as $subscription): ?>
			<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
			<tr class="<?php echo $class; ?>">
				<td><?php echo $subscription['Subscription']['id']; ?>&nbsp;</td>
				<td><?php echo $subscription['Subscription']['full_name']; ?>&nbsp;</td>
				<td><?php echo $subscription['Subscription']['email']; ?>&nbsp;</td>
				<td><?php echo $this->Format->boolean($subscription['Subscription']['is_subscribed']); ?>&nbsp;</td>
				<td class="text-right">
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-eye-open"></span> ' . __('View'), array('action' => 'view', $subscription['Subscription']['id']), array('class' => 'btn btn-primary')); ?>
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> ' . __('Edit'), array('action' => 'edit', $subscription['Subscription']['id']), array('class' => 'btn btn-primary')); ?>
					<?php echo $this->Html->link('<span class="glyphicon glyphicon-remove"></span> ' . __('Delete'), array('action' => 'delete', $subscription['Subscription']['id']), array('class' => 'btn btn-primary'), __('Are you sure you want to delete # %s?', $subscription['Subscription']['id'])); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
	</div>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="col-md-2 panel-default aside">
	<h3 class="panel-heading"><?php echo __('Actions'); ?></h3>
	<ul class="list-unstyled">
		<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span> ' . __('New Subscription'), array('action' => 'add'), array('class' => 'btn btn-primary')); ?></li>
		<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-download"></span> ' . __('Export Subscription'), array('action' => 'export'), array('class' => 'btn btn-primary')); ?></li>
	</ul>
</div>