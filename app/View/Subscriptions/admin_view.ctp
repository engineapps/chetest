<div class="content panel panel-default col-md-10">
	<h3 class="panel-heading"><?php  echo __('Subscription'); ?></h3>
	<dl class="dl-horizontal"><?php $i = 0; $class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $subscription['Subscription']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Full Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $subscription['Subscription']['full_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Email'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $subscription['Subscription']['email']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Is Subscribed'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Format->boolean($subscription['Subscription']['is_subscribed']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($subscription['Subscription']['created']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($subscription['Subscription']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
	
	<h3 class="panel-heading"><?php echo __('Related Communities'); ?></h3>
	<?php if (!empty($subscription['CommunitySubscription'])): ?>
	<div class="table-responsive">
		<table class="table table-striped">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<th><?php echo __('Name'); ?></th>
			<th class="text-right"><?php echo __('Is Subscribed'); ?></th>
		</tr>
		<?php $i = 0; ?>
		<?php foreach ($subscription['CommunitySubscription'] as $communitySubscription): ?>
		<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
			<tr class="<?php echo $class; ?>">
				<td><?php echo $communitySubscription['Community']['id']; ?></td>
				<td><?php echo $communitySubscription['Community']['name']; ?></td>
				<td class="text-right">
					<?php echo $communitySubscription['is_subscribed']; ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	</div>
	<?php endif; ?>
</div>
<div class="col-md-2 panel-default aside">
	<h3 class="panel-heading"><?php echo __('Actions'); ?></h3>
	<ul class="list-unstyled">
		<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span> ' . __('Edit Subscription'), array('action' => 'edit', $subscription['Subscription']['id']), array('class' => 'btn btn-primary')); ?></li>
		<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-remove"></span> ' . __('Delete Subscription'), array('action' => 'delete', $subscription['Subscription']['id']), array('class' => 'btn btn-primary'), __('Are you sure you want to delete # %s?', $subscription['Subscription']['id'])); ?></li>
		<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-list"></span> ' . __('List Subscriptions'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?></li>
		<li><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span> ' . __('New Subscription'), array('action' => 'add'), array('class' => 'btn btn-primary')); ?></li>
	</ul>
</div>