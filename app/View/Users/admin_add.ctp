<div class="content">
	<?php echo $this->Form->create('User'); ?>
	<h1><?php echo __('Admin Add User'); ?></h1>
	<fieldset>
	<?php
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('email');
		echo $this->Form->input('password');
		echo $this->Form->input('phone');
		echo $this->Form->input('level', array('options' => User::$userLevels));
		echo $this->Form->input('group_id');
		echo $this->Form->input('is_active', array('checked' => 'checked'));
		echo $this->Form->input('is_subscribed', array('checked' => 'checked'));
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>