<div class="content">
	<h1><?php echo __('Users'); ?></h1>
	<table>
	<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('full_name'); ?></th>
		<th><?php echo $this->Paginator->sort('email'); ?></th>
		<th><?php echo $this->Paginator->sort('logged'); ?></th>
		<th><?php echo $this->Paginator->sort('level'); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($users as $user): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $user['User']['id']; ?>&nbsp;</td>
		<td><?php echo $user['User']['full_name']; ?>&nbsp;</td>
		<td><?php echo $user['User']['email']; ?>&nbsp;</td>
		<td><?php echo $this->Time->niceShort($user['User']['logged']); ?>&nbsp;</td>
		<td><?php echo User::$userLevels[$user['User']['level']]; ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $user['User']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>