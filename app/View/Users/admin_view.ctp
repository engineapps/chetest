<div class="content">
	<h1><?php  echo __('User'); ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user['User']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Full Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user['User']['full_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Email'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user['User']['email']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Phone'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user['User']['phone']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($user['User']['modified']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Logged'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($user['User']['logged']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Group'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Level'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo User::$userLevels[$user['User']['level']]; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id']), array('class' => 'edit')); ?></li>
		<li><?php echo $this->Html->link(__('Delete User'), array('action' => 'delete', $user['User']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index'), array('class' => 'list')); ?></li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>

<div class="related">
	<h1><?php echo __('Related Addresses'); ?></h1>
	<?php if (!empty($user['Address'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($user['Address'] as $address): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
		<tr class="<?php echo $class; ?>">
			<td><?php echo $address['id']; ?></td>
			<td><?php echo Address::format(array('Address' => $address)); ?></td>
			<td><?php echo $this->Time->niceShort($address['created']); ?></td>
			<td class="text-right">
				<?php echo $this->Html->link(__('Delete'), array('controller' => 'addresses', 'action' => 'delete', $address['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $address['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php endif; ?>
	<h1><?php echo __('Related Orders'); ?></h1>
	<?php if (!empty($user['Order'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Order'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($user['Order'] as $order): ?>
	<?php $cart = unserialize($order['data']); ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
		<tr class="<?php echo $class; ?>">
			<td><?php echo $order['id']; ?></td>
			<td>
				<?php if (!empty($cart['Pending'])): ?>
					<h3><?php echo __("Following items are not added to the cart, follow links below to select all options."); ?></h3>
					<?php foreach ($cart['Pending'] as $item): ?>
						<div class="flashMessage notice">
						<?php echo $this->Html->link($item['Parent']['name'], array('controller' => 'products', 'action' => 'view', $item['Parent']['missing_item'], $item['Parent']['hash'])); ?>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
				<?php if (!empty($cart['Product'])): ?>
				<table>
					<tr>
						<th class="text-left"><?php echo __('Product'); ?></th>
						<th class="text-right"><?php echo __('Unit Price'); ?></th>
						<th class="text-right"><?php echo __('Quantity'); ?></th>
						<th class="text-right"><?php echo __('Price'); ?></th>
					</tr>
					<?php foreach ($cart['Product'] as $product): ?>
						<tr>
							<td>
								<?php echo $product['Parent']['name']; ?><br />
								<ul class="options">
								<?php if(!empty($product['Parent']['all_modifiers'])): ?>
									<?php $modifiers = Set::extract('/1', $product['Parent']['all_modifiers']); ?>
									<li><?php echo implode(', ', $modifiers); ?></li>
								<?php endif; ?>
								<?php if(!empty($product['Parent']['crust'])): ?>
									<li><?php echo __('Crust: %s', current($product['Parent']['crust'])); ?></li>
								<?php endif; ?>
								<?php if(!empty($product['Parent']['cook'])): ?>
									<li><?php echo __('Cook: %s', current($product['Parent']['cook'])); ?></li>
								<?php endif; ?>
								<?php if(!empty($product['Parent']['sauce'])): ?>
									<li><?php echo __('Sauce: %s', current($product['Parent']['sauce'])); ?></li>
								<?php endif; ?>
								<?php if(!empty($product['Parent']['panz'])): ?>
									<li><?php echo __('Panz: %s', current($product['Parent']['panz'])); ?></li>
								<?php endif; ?>
								<?php if(!empty($product['Parent']['toppings'])): ?>
									<?php if(!empty($product['Parent']['toppings']['RH'])): ?>
									<li><?php echo __('Right Half: %s', implode(', ', $product['Parent']['toppings']['RH'])); ?></li>
									<?php endif; ?>
									<?php if(!empty($product['Parent']['toppings']['LH'])): ?>
									<li><?php echo __('Left Half: %s', implode(', ', $product['Parent']['toppings']['LH'])); ?></li>
									<?php endif; ?>
									<?php if(!empty($product['Parent']['toppings']['WP'])): ?>
									<li><?php echo __('On Whole: %s', implode(', ', $product['Parent']['toppings']['WP'])); ?></li>
									<?php endif; ?>
								<?php endif; ?>
								</ul>
								<?php if(!empty($product['Children'])): ?>
									<?php foreach($product['Children'] as $item): ?>
										<?php echo $item['name']; ?><br />
										<ul class="options">
										<?php if(!empty($item['all_modifiers'])): ?>
											<?php $modifiers = Set::extract('/1', $item['all_modifiers']); ?>
											<li><?php echo implode(', ', $modifiers); ?></li>
										<?php endif; ?>
										<?php if(!empty($item['crust'])): ?>
											<li><?php echo __('Crust: %s', current($item['crust'])); ?></li>
										<?php endif; ?>
										<?php if(!empty($item['cook'])): ?>
											<li><?php echo __('Cook: %s', current($item['cook'])); ?></li>
										<?php endif; ?>
										<?php if(!empty($item['sauce'])): ?>
											<li><?php echo __('Sauce: %s', current($item['sauce'])); ?></li>
										<?php endif; ?>
										<?php if(!empty($item['panz'])): ?>
											<li><?php echo __('Panz: %s', current($item['panz'])); ?></li>
										<?php endif; ?>
										<?php if(!empty($item['toppings'])): ?>
											<?php if(!empty($item['toppings']['RH'])): ?>
												<li><?php echo __('Right Half: %s', implode(', ', $item['toppings']['RH'])); ?></li>
											<?php endif; ?>
											<?php if(!empty($item['toppings']['LH'])): ?>
												<li><?php echo __('Left Half: %s', implode(', ', $item['toppings']['LH'])); ?></li>
											<?php endif; ?>
											<?php if(!empty($item['toppings']['WP'])): ?>
												<li><?php echo __('On Whole: %s', implode(', ', $item['toppings']['WP'])); ?></li>
											<?php endif; ?>
										<?php endif; ?>
										</ul>
									<?php endforeach; ?>
								<?php endif; ?>
							</td>
							<td class="text-right"><?php echo $this->Number->currency($product['Parent']['price'], 'USD'); ?>&nbsp;</td>
							<td class="text-right">
								<?php echo $product['Parent']['qty']; ?>
							</td>
							<td class="text-right"><?php echo $this->Number->currency($product['Parent']['price'] * $product['Parent']['qty'], 'USD'); ?>&nbsp;</td>
						</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="4" class="text-right">
							<?php echo __('Sub Total : %s', $this->Number->currency($cart['Total']['subtotal'], 'USD')); ?><br />
							<?php if($cart['Total']['discounts'] > 0): ?>
								<?php echo __('Discount : -%s', $this->Number->currency($cart['Total']['discounts'], 'USD')); ?><br />
							<?php endif; ?>
							<?php echo __('Delivery : %s', $this->Number->currency($cart['Total']['delivery'], 'USD', array('zero' => __('Free')))); ?><br />
							<?php echo __('Taxes : %s', $this->Number->currency($cart['Total']['taxes'], 'USD')); ?><br />
							<?php if($cart['Total']['giftcards'] > 0): ?>
								<?php echo __('Gift Cards : -%s', $this->Number->currency($cart['Total']['giftcards'], 'USD')); ?><br />
							<?php endif; ?>
							<?php echo __('Net Payable : %s', $this->Number->currency($cart['Total']['payable'], 'USD')); ?>
						</td>
					</tr>
				</table>
				<?php endif; ?>
			</td>
			<td><?php echo $this->Time->niceShort($order['created']); ?></td>
			<td class="text-right">
				<?php echo $this->Html->link(__('View'), array('controller' => 'orders', 'action' => 'view', $order['id']), array('class' => 'view')); ?>
				<?php echo $this->Html->link(__('Delete'), array('controller' => 'orders', 'action' => 'delete', $order['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $order['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php endif; ?>
</div>