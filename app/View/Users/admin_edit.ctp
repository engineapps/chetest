<div class="content">
	<?php echo $this->Form->create('User'); ?>
	<h1><?php echo __('Admin Edit User'); ?></h1>
	<fieldset>
		<?php
		echo $this->Form->input('id');
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('email');
		echo $this->Form->input('new_password', array('type' => 'password', 'after' => __('<pre>Leave blank if unchanged<pre>'), 'escape' => false));
		echo $this->Form->input('phone');
		echo $this->Form->input('level', array('options' => User::$userLevels));
		echo $this->Form->input('group_id');
		echo $this->Form->input('is_active');
		echo $this->Form->input('is_subscribed');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>