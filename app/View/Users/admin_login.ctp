<div id="LoginWindow">
	<?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'login'))); ?>
	<div class="contents">
		<div class="flashMessage notice"><?php echo __("Welcome to Admin Panel, enter your details to login."); ?></div>
	</div>
	<?php echo $this->Session->flash(); ?>
	<?php 
		echo $this->Form->input('email');
		echo $this->Form->input('password');
		echo $this->Form->submit(__('Login Now'), array('class' => 'button'));
	?>
	<?php echo $this->Form->end(); ?>
	<?php echo $this->Html->image('engine_logo.png'); ?>
</div>