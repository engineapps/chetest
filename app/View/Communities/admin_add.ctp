<div class="content">
	<?php echo $this->Form->create('Community', array('type' => 'file')); ?>
	<h1><?php echo __('Admin Add Community'); ?></h1>
	<fieldset>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('slug');
		echo $this->Form->input('title');
		echo $this->Form->input('slogan');
		echo $this->Form->input('seo_title');
		echo $this->Form->input('seo_keywords');
		echo $this->Form->input('seo_description');
		echo $this->Form->input('logo', array('type' => 'file'));
		echo $this->Form->input('siteplan', array('type' => 'file'));
		echo $this->Form->input('description');
		echo $this->Form->input('type');
		echo $this->Form->input('grid', array('options' => $grids));
		echo $this->Form->input('office_id', array('empty' => __('-- Select Office --')));
		echo $this->Form->input('is_sold');
		echo $this->Form->input('is_upcoming');
		echo $this->Form->input('is_active', array('checked' => true));
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Communities'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>