<div class="content">
	<?php echo $this->Form->create('Community', array('type' => 'file')); ?>
	<h1><?php echo __('Admin Edit Community'); ?></h1>
	<fieldset>
		<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('slug');
		echo $this->Form->input('title');
		echo $this->Form->input('slogan');
		echo $this->Form->input('seo_title');
		echo $this->Form->input('seo_keywords');
		echo $this->Form->input('seo_description');
		echo $this->Form->input('logo', array('type' => 'file', 'after' => $this->Html->image('/thumbs/img/communities/100____0____' . $this->request->data['Community']['logo'])));
		echo $this->Form->input('siteplan', array('type' => 'file', 'after' => $this->Html->image('/thumbs/img/communities/100____0____' . $this->request->data['Community']['siteplan'])));
		echo $this->Form->input('description');
		echo $this->Form->input('type');
		echo $this->Form->input('grid', array('options' => $grids));
		echo $this->Form->input('office_id', array('empty' => __('-- Select Office --')));
		echo $this->Form->input('is_sold');
		echo $this->Form->input('is_upcoming');
		echo $this->Form->input('is_active');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $this->Form->value('Community.id')), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $this->Form->value('Community.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Communities'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>