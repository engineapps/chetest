<div class="content">
	<h1><?php echo __('Communities'); ?></h1>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('is_active'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th class="text-right"><?php echo __('Actions'); ?></th>
		</tr>
		<?php $i = 0; ?>
		<?php foreach ($communities as $community): ?>
			<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
			<tr class="<?php echo $class; ?>">
				<td><?php echo $community['Community']['id']; ?>&nbsp;</td>
				<td><?php echo $community['Community']['name']; ?>&nbsp;</td>
				<td><?php echo $community['Community']['is_active']; ?>&nbsp;</td>
				<td><?php echo $community['Community']['type']; ?></td>
				<td class="text-right">
					<?php echo $this->Html->link(__('1'), array('action' => 'moveup', $community['Community']['id'], 1), array('class' => 'up')); ?>
					<?php echo $this->Html->link(__('1'), array('action' => 'movedown', $community['Community']['id'], 1), array('class' => 'down')); ?>
					<?php echo $this->Html->link(__('Homes'), array('controller' => 'homes', $community['Community']['id']), array('class' => 'list')); ?>
					<?php echo $this->Html->link(__('Gallery'), array('controller' => 'photos', $community['Community']['id']), array('class' => 'list')); ?>
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $community['Community']['id']), array('class' => 'view')); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $community['Community']['id']), array('class' => 'edit')); ?>
					<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $community['Community']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $community['Community']['id'])); ?>

				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Community'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>