<div class="row community">
	<h1 class="page-title"><?php echo $community['Community']['title']; ?></h1>
	<div class="page-intro"><?php echo $community['Community']['formatted']; ?></div>
	<div id="community-headlines" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active"><?php echo $this->Html->image('communities/' . $community['Community']['logo'], array('class' => 'img-responsive')); ?></div>
		</div>
	</div>
</div>
<?php if(!empty($homes)): ?>
<hr class="fancy-hr">
<div class="row plans">
	<h2 class="section-title" id="plans-and-pricing"><?php echo __('Plans and Pricing'); ?></h2>
	<?php foreach ($homes as $home): ?>
		<?php foreach ($home['Image'] as $image): ?>
			<?php if($image['Elevation']['display']): ?>
				<div class="home">
					<div class="home-wrap">
						<div class="row">
							<div class="thumb-wrap">
								<a href="<?php echo $this->Html->url(array('controller' => 'homes', 'action' => 'view', $home['Home']['slug'], $image['id'])); ?>"><?php echo $this->Html->image('/thumbs/img/homes/325____0____' . $image['name'], array('class' => 'img-responsive')); ?></a>
							</div>
							<div class="home-details">
								<h4><a href="<?php echo $this->Html->url(array('controller' => 'homes', 'action' => 'view', $home['Home']['slug'], $image['id'])); ?>"><?php echo $home['Home']['name']; ?> <?php echo ($image['is_sold']) ? __('(Sold out)') : ''; ?></a></h4>
								<a href="<?php echo $this->Html->url(array('controller' => 'homes', 'action' => 'view', $home['Home']['slug'], $image['id'])); ?>"><span class="specs"><?php echo $home['Style']['name']; ?><br><?php echo $image['size']; ?> SQ. FT.</span></a>
							</div>
						</div>
					</div>
				</div>
				<?php break; ?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endforeach; ?>
</div>
<?php endif; ?>
<?php if(!empty($community['Community']['siteplan'])): ?>
<hr class="fancy-hr">
<div class="row sitemap">
	<h2 class="section-title"><?php echo __('Site PLan &amp; Location'); ?></h2>
	<div class="plan">
		<div class="plan-wrap">
			<?php echo $this->Html->image('communities/' . $community['Community']['siteplan'], array('class' => 'img-responsive')); ?>
		</div>
	</div>
</div>
<? endif; ?>
<?php if(!empty($community['Office']['id'])): ?>
<hr class="fancy-hr">
<div class="row offices">
	<h2 class="section-title"><?php echo __('Contact ') . $community['Community']['name']; ?></h2>
	<div class="office">
		<div class="office-wrap">
			<div class="row">
				<div class="address">
					<h2 class="office-title">Contact Info</h2>
					<?php echo $community['Office']['description']; ?>
					<hr class="visible-xs">
				</div>
				<div class="hours">
					<h2 class="office-title">Hours of Operation</h2>
					<?php echo $community['Office']['hours']; ?>
					<hr class="visible-xs">
				</div>
				<div class="keymap">
					<?php if (!empty($community['Office']['map'])): ?>
					<?php echo $this->Html->image('offices/' . $community['Office']['map'], array('class' => 'img-responsive')); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<? endif; ?>