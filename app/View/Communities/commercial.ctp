<div class="row communities">
	<h1 class="page-title"><?php echo __('Business Before Pleasure'); ?></h1>
	<div class="page-intro"><?php echo __('Tempor platea aliquam et tincidunt scelerisque nunc aliquam duis! Integer porta magnis. Lectus scelerisque porta augue odio amet sit ac mid montes nec et magna urna, nunc, sit vel pid tincidunt est, sit nunc sagittis? A lacus sagittis, mus ac. Urna integer.'); ?></div>
</div>
<?php if(!empty($communities)): ?>
<hr class="fancy-hr">
<div class="row featured-communities">
	<h2 class="section-title"><?php echo __('Featured Communities'); ?></h2>
	<?php $counter = 1; ?>
	<?php foreach ($communities as $community): ?>
	<div class="community-<?php echo $counter % 3 === 0 || $counter  % 4 === 0 ? 'thin' : 'wide'; ?>">
		<div class="clippers">
			<?php echo $this->Html->image('/thumbs/img/communities/458____0____' . $community['Community']['logo']); ?>
			
			<a href="<?php echo $this->Html->url(array('action' => 'view', $community['Community']['slug'])); ?>" class="community-name"><span><?php echo $community['Community']['name']; ?></span></a>
		</div>
	</div>
	<?php
	if($counter === 6) {
		$counter = 0;
	} else {
		$counter ++;
	}
	?>
	<?php endforeach; ?>
</div>
<?php endif; ?>