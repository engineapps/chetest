<div class="content">
<h1><?php echo __('Community'); ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $community['Community']['id']; ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $community['Community']['name']; ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Slug'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $community['Community']['slug']; ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $community['Community']['title']; ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Seo Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $community['Community']['seo_title']; ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Seo Keywords'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $community['Community']['seo_keywords']; ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Seo Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $community['Community']['seo_description']; ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Logo'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Html->image('/thumbs/img/communities/100____0____'.$community['Community']['logo']); ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $community['Community']['formatted']; ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $community['Community']['type']; ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Is Sold'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $community['Community']['is_sold']; ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Is Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $community['Community']['is_active']; ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($community['Community']['created']); ?>&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($community['Community']['modified']); ?>&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Community'), array('action' => 'edit', $community['Community']['id']), array('class' => 'edit')); ?></li>
		<li><?php echo $this->Html->link(__('Delete Community'), array('action' => 'delete', $community['Community']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $community['Community']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('List Communities'), array('action' => 'index'), array('class' => 'list')); ?></li>
		<li><?php echo $this->Html->link(__('New Community'), array('action' => 'add'), array('class' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Export Registrations'), array('controller' => 'registrations', 'action' => 'export', $community['Community']['id'], 'unique'), array('class' => 'export')); ?></li>
	</ul>
</div>
<div class="related">
	<h1><?php echo __('Related Homes'); ?></h1>
	<?php if (!empty($community['Home'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($community['Home'] as $home): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $home['id']; ?>&nbsp;</td>
		<td><?php echo $home['name']; ?>&nbsp;</td>
		<td><?php echo $this->Time->niceShort($home['created']); ?>&nbsp;</td>
		<td><?php echo $this->Time->niceShort($home['modified']); ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('Images'), array('controller' => 'images', 'action' => 'index', $home['id']), array('class' => 'image')); ?>
			<?php echo $this->Html->link(__('View'), array('controller' => 'homes', 'action' => 'view', $home['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Edit'), array('controller' => 'homes', 'action' => 'edit', $home['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('Delete'), array('controller' => 'homes', 'action' => 'delete', $home['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $home['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php endif; ?>
</div>
