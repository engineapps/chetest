
<div class="content">
	<h1><?php echo __('Communities'); ?></h1>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('is_active'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('registrants'); ?></th>
			<th class="text-right"><?php echo __('Actions'); ?></th>
		</tr>
		<?php $i = 0; ?>
		<?php foreach ($communities as $community): ?>
			<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
			<tr class="<?php echo $class; ?>">
				<td><?php echo $community['Community']['id']; ?>&nbsp;</td>
				<td><?php echo $community['Community']['name']; ?>&nbsp;</td>
				<td><?php echo $community['Community']['is_active']; ?>&nbsp;</td>
				<td><?php echo $community['Community']['type']; ?></td>
				<td style="text-align:center;"><?php echo count($community['CommunityRegistration']); ?></td>
				<td class="text-right">
					<?php echo $this->Html->link(
							__('Export Registrations'),
							array(
									'controller' => 'registrations',
									'action' => 'export',
									$community['Community']['id'], 'unique'
							),
							array('class' => 'export')); ?>

				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">


</div>