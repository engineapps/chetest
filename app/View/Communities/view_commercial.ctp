<div class="row community">
	<h1 class="page-title"><?php echo $community['Community']['title']; ?></h1>
</div>
<div class="row page sitemap">
	<div class="plan">
		<div class="plan-wrap">
			<?php if(!empty($community['Office']['id'])): ?>
			<div id="map_canvas" class="clearfix" data-toggle="map" data-lat="<?php echo $community['Office']['lat']; ?>" data-lng="<?php echo $community['Office']['lng'] ?>" data-content="Map"></div>
			<?php else: ?>
			<?php echo $this->Html->image('communities/' . $community['Community']['logo'], array('class' => 'img-responsive')); ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<br>
<div class="row community">
	<?php echo $community['Community']['formatted']; ?>
</div>
<div class="row model-homes">
	<hr class="fancy-hr">
	<?php if (!empty($community['Photo'])): ?>
	
			<?php foreach ($community['Photo'] as $photo): ?>
				<?php if ($photo['is_gallery']): ?>
					
					<div class="col-xs-6 col-sm-4 col-lg-3">
						<div class="image">
							<a href="<?php echo $this->Html->url('/img/communities/' . $photo['name']); ?>" class="fancybox" rel="<?php echo $community['Community']['title']; ?>">
								<?php echo $this->Html->image('communities/' . $photo['name'], array('class' => 'img-responsive')); ?>
							</a>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>

	<?php endif; ?>
</div>
<div class="row home-single ctas">
	<div class="home-btn">
		<a href="mailto:<?php echo $appConfig['email']; ?>?subject=<?php echo $community['Community']['title']; ?>&amp;body=I am interested in receiving further information regarding <?php echo $community['Community']['title']; ?>" class="btn btn-primary btn-block" rel="nofollow"><?php echo __('Request Info about this property'); ?></a>
	</div>
</div>
