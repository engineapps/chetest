<div class="row communities">
	<h1 class="page-title"><?php echo $page['Page']['title']; ?></h1>
	<div class="page-intro"><?php echo $page['Page']['contents']; ?></div>
	<?php if (!empty($slides)): ?>
		<div id="community-headlines" class="carousel slide" data-ride="carousel">
			<?php
			/*
			 *
			 * <!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>
			 */
			?>
			<div class="carousel-inner">
				<?php $counter = 0; ?>
				<?php foreach ($slides as $slide): ?>
				<?php $class = $counter === 0 ? 'item active' : 'item'; ?>
				<div class="<?php echo $class; ?>">
					<?php echo $this->Html->image('slides/' . $slide['Slide']['image'], array('class' => 'img-responsive')); ?>
					<div class="descr">
						<h5><?php echo $slide['Slide']['title']; ?></h5>
						<span><?php echo $slide['Slide']['description']; ?></span>
					</div>
				</div>
				<?php $counter++; ?>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="community-intro">
		<h2 class="section-title"><?php echo $appConfig['name']; ?></h2>
		<p><?php echo __('Vogue Homes has been building the finest quality homes and subdivision homes for over 25 years and has established a premier reputation of being one of the finest homebuilders in the Greater Toronto Area.'); ?></p>
	</div>
	<div class="community-social">
		<h2 class="section-title"><?php echo __('Get Social'); ?></h2>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=136417169876126&version=v2.0";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<div class="row">
			<div class="social-widget">
				<div class="dark-overlay">
					<div class="social-icon visible-lg"><a href="<?php echo $appConfig['Facebook']['url']; ?>" target="_blank"><?php echo $this->Html->image('icons/fbook.png', array('class' => 'img-responsive')); ?></a></div>
					<a href="<?php echo $appConfig['Facebook']['url']; ?>" class="widget-title fbook-title" target="_blank"><?php echo __('The Vogue Development<br>Group Inc.'); ?></a>
					<div class="fb-like" data-href="https://www.facebook.com/VogueDevelopmentGroup" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
				</div>
			</div>
			<div class="social-widget">
				<div class="dark-overlay">
					<div class="social-icon visible-lg reg-icon"><a href="<?php echo $this->Html->url(array('controller' => 'register', 'action' => 'add')); ?>"><?php echo $this->Html->image('icons/reg-icon.png', array('class' => 'img-responsive')); ?></a></div>
					<a href="<?php echo $this->Html->url(array('controller' => 'register', 'action' => 'add')); ?>" class="widget-title reg-title"><?php echo __('Register for<br>Community Updates'); ?></a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if(!empty($communities)): ?>
<hr class="fancy-hr">
<div class="row featured-communities">
	<h2 class="section-title"><?php echo __('Featured Communities'); ?></h2>
	<?php $counter = 1; ?>
	<?php foreach ($communities as $community): ?>
	<div class="community-<?php echo $counter % 3 === 0 || $counter  % 4 === 0 ? 'thin' : 'wide'; ?>">
		<div class="clippers">
			<?php echo $this->Html->image('/thumbs/img/communities/458____0____' . $community['Community']['logo']); ?>
			
			<a href="<?php echo $this->Html->url(array('action' => 'view', $community['Community']['slug'])); ?>" class="community-name"><span><?php echo $community['Community']['name']; ?></span></a>
		</div>
	</div>
	<?php
	if($counter === 6) {
		$counter = 0;
	} else {
		$counter ++;
	}
	?>
	<?php endforeach; ?>
</div>
<?php endif; ?>