<div class="row contact">
	<h1 class="page-title"><?php echo $page['Page']['name']; ?></h1>
	
</div>
<div class="row page sitemap relative">
	<div class="plan">
		<div class="plan-wrap">
			<div id="map_canvas" class="clearfix" data-toggle="map" data-lat="<?php echo $appConfig['Address']['geo']['lat']; ?>" data-lng="<?php echo $appConfig['Address']['geo']['lng']; ?>" data-content="Map"></div>
		</div>
	</div>
	<div class="contact-info clearfix">
		<div class="col-sm-3"><span class="glyphicon glyphicon-envelope"></span>&nbsp;<a href="mailto:<?php echo $appConfig['email']; ?>" rel="nofollow"><?php echo $appConfig['email']; ?></a></div>
		<div class="col-sm-5"><span class="glyphicon glyphicon-map-marker"></span>&nbsp;<a href="https://goo.gl/maps/Bs7V5" rel="nofollow" target="_blank"><?php echo __('%s, %s, %s, %s', $appConfig['Address']['address'], $appConfig['Address']['city'], $appConfig['Address']['province'], $appConfig['Address']['postal']); ?></a></div>
		<div class="col-sm-4" style="text-align: right;">
			<ul class="social">
				<li><?php echo $this->Html->link(__('Facebook'), $appConfig['Facebook']['url'], array('class' => 'facebook', 'target' => '_blank')); ?></li>
			</ul>
		</div>
	</div>
</div>