<div class="content">
	<h1><?php echo __('Pages'); ?></h1>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="text-right"><?php echo __('Actions'); ?></th>
		</tr>
		<?php $i = 0; ?>
		<?php foreach ($pages as $page): ?>
		<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
		<tr class="<?php echo $class; ?>">
			<td><?php echo $page['Page']['id']; ?>&nbsp;</td>
			<td><?php echo $this->Html->link($page['Page']['name'], array('action' => 'view', $page['Page']['id'])); ?>&nbsp;</td>
			<td><?php echo $this->Time->niceShort($page['Page']['modified']); ?>&nbsp;</td>
			<td class="text-right">
				<?php echo $this->Html->link(__('View'), array('action' => 'view', $page['Page']['id']), array('class' => 'view')); ?>
				<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $page['Page']['id']), array('class' => 'edit')); ?>
				<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $page['Page']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $page['Page']['id'])); ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Page'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>