<div class="row">
	<h1 class="page-title"><?php echo $page['Page']['name']; ?></h1>
	<!--<div class="page-intro"><?php echo $page['Page']['contents']; ?></div>-->
</div>
<div class="row model-homes">
	<hr class="fancy-hr">
	<?php if (!empty($slides)): ?>
	<div class="col-xs-12">
		<div class="masonry-2 js-masonry">
			<?php foreach ($slides as $slide): ?>
				<div class="item">
					<div class="image">
						<a href="<?php echo $this->Html->url('/img/slides/' . $slide['Slide']['image']); ?>" class="fancybox" rel="<?php echo $page['Page']['name']; ?>">
							<?php echo $this->Html->image('slides/' . $slide['Slide']['image'], array('class' => 'img-responsive')); ?>
						</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="clear"></div>
	</div>
	<?php endif; ?>
</div>