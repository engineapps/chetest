<div class="content">
	<?php echo $this->Form->create('Page', array('type' => 'file')); ?>
	<h1><?php echo __('Admin Edit Page'); ?></h1>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('title');
		echo $this->Form->input('slug');
		echo $this->Form->input('seo_title');
		echo $this->Form->input('seo_keywords');
		echo $this->Form->input('seo_description');
		echo $this->Form->input('banner', array('type' => 'file', 'after' => $this->Html->image('/thumbs/img/pages/100____0____'.$this->request->data['Page']['banner'])));
		echo $this->Form->input('contents', array('cols' => '15'));
		echo $this->Form->input('is_searchable');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $this->Form->value('Page.id')), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $this->Form->value('Page.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Pages'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>