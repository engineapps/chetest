<div class="content">
	<h1><?php echo __('Admin Panel'); ?></h1>
	<?php if (!empty($q)): ?>
		<h1><?php echo __('Search Results for <b>"%s"</b>', $q); ?></h1>
		<?php if (!empty($results)): ?>
			<?php foreach ($results as $model => $result): ?>
				<?php if (!empty($result)): ?>
					<h1><?php echo $model; ?></h1>
					<table class="box static">
						<thead>
							<tr>
								<?php foreach ($models[$model] as $field): ?>
									<th><?php echo Inflector::humanize($field); ?></th>
								<?php endforeach; ?>
								<th class="text-right"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($result as $item): ?>
								<tr>
									<?php foreach ($models[$model] as $field): ?>
										<td><?php echo $item[$model][$field]; ?></td>
									<?php endforeach; ?>
									<td class="text-right">
										<?php echo $this->Html->link(__('View'), array('controller' => Inflector::pluralize(strtolower($model)), 'action' => 'view', $item[$model]['id']), array('class' => 'view')); ?>
										<?php echo $this->Html->link(__('Edit'), array('controller' => Inflector::pluralize(strtolower($model)), 'action' => 'edit', $item[$model]['id']), array('class' => 'edit')); ?>
										<?php echo $this->Html->link(__('Delete'), array('controller' => Inflector::pluralize(strtolower($model)), 'action' => 'delete', $item[$model]['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $item[$model]['id'])); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	<?php else: ?>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
			google.load('visualization', '1.0', {'packages':['corechart']});
			google.setOnLoadCallback(drawAnalytics);
			function drawAnalytics() {
				drawPageviews();
				drawVisits()
				drawPercentages();
			}
			function drawPageviews() {
				var data = new google.visualization.arrayToDataTable([
					<?php echo implode(', ', $graph['Pageviews']); ?>
				]);

				var options = {
					title: '<?php echo __("Daily Pageviews"); ?>'
				};

				var chart = new google.visualization.AreaChart(document.getElementById('gaPageviews'));
				chart.draw(data, options);
			}
			function drawVisits() {
				var data = new google.visualization.arrayToDataTable([
					<?php echo implode(', ', $graph['Visits']); ?>
				]);

				var options = {
					title: '<?php echo __("Daily Visits"); ?>'
				};

				var chart = new google.visualization.AreaChart(document.getElementById('gaVisits'));
				chart.draw(data, options);
			}
			function drawPercentages() {
				var data = new google.visualization.arrayToDataTable([
					<?php echo implode(', ', $graph['Percentages']); ?>
				]);

				var options = {
					title: '<?php echo __("Today Stats"); ?>'
				};

				var chart = new google.visualization.PieChart(document.getElementById('gaPercentages'));
				chart.draw(data, options);
			}
		</script>
		<div id="gaVisits"></div>
		<div id="gaPageviews"></div>
		<div style="width: 48%; float: left;" id="gaPercentages"></div>
		<div style="width: 48%; float: right; background: #fff; padding: 10px; min-height: 180px;" id="gaData">
			<?php echo __('Bounce Rate: %s', $graph['VisitBounceRate'] . '%'); ?>
		</div>
		<br class="clear" />
	<?php endif; ?>
</div>