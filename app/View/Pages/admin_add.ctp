<div class="content">
	<?php echo $this->Form->create('Page', array('type' => 'file')); ?>
	<h1><?php echo __('Admin Add Page'); ?></h1>
	<fieldset>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('title');
		echo $this->Form->input('slug');
		echo $this->Form->input('seo_title');
		echo $this->Form->input('seo_keywords');
		echo $this->Form->input('seo_description');
		echo $this->Form->input('banner', array('type' => 'file'));
		echo $this->Form->input('contents', array('cols' => '15'));
		echo $this->Form->input('is_searchable');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Pages'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>