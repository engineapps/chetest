<div class="content">
	<h1><?php echo __('Slides'); ?></h1>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo __('Slide'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo $this->Paginator->sort('Page.name', __('Page')); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($slides as $slide): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow ' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $slide['Slide']['id']; ?>&nbsp;</td>
		<td><?php echo $this->Html->image('/thumbs/img/slides/100____0____'.$slide['Slide']['image']); ?></td>
		<td><?php echo $slide['Slide']['title']; ?>&nbsp;</td>
		<td><?php echo !empty($slide['Page']['title']) ? $slide['Page']['title'] : __('Homepage'); ?>&nbsp;</td>
		<td><?php echo $slide['Slide']['is_active']; ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('1'), array('action' => 'moveup', $slide['Slide']['id'], '1'), array('class' => 'up')); ?>
			<?php echo $this->Html->link(__('1'), array('action' => 'movedown', $slide['Slide']['id'], '1'), array('class' => 'down')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $slide['Slide']['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $slide['Slide']['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $slide['Slide']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $slide['Slide']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Slide'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>