<div class="content">
	<h1><?php echo __('Admin Add Slide'); ?></h1>
	<?php echo $this->Form->create('Slide', array('type' => 'file')); ?>
	<fieldset>
		<?php
		echo $this->Form->input('title');
		echo $this->Form->input('link', array('after' => 'e.g. /page/contact'));
		echo $this->Form->input('image', array('type' => 'file'));
		echo $this->Form->input('background');
		echo $this->Form->input('twitter_hash');
		echo $this->Form->input('description', array('class' => 'noEditor'));
		echo $this->Form->input('page_id');
		echo $this->Form->input('is_active');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Slides'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>