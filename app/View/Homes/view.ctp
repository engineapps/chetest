<div class="row home-single">
	<h2 class="section-title"><?php echo $home['Community']['name']; ?></h2>
	<h1 class="home-title">
		<span><?php echo $home['Home']['name']; ?> &mdash; <?php echo $image['Elevation']['name']; ?></span>
		<?php echo $image['Image']['size']; ?> SQ. FT.
	</h1>
	<div class="home-stats">
		<?php echo __('Bedrooms: ') . '<strong>' . $home['Home']['bedrooms'] . '</strong>'; ?><br>
		<?php echo __('Bathrooms: ') . '<strong>' . $home['Home']['bathrooms'] . '</strong>'; ?><br>
		<?php echo __('Property Type: ') . '<strong>' . $home['Style']['name'] . '</strong>'; ?><br>
	</div>
</div>
<div class="row home-single">
	<div class="elevations-nav">
		<hr class="visible-xs">
		<ul class="nav nav-pills">
		<?php $counter = 0; ?>
		<?php foreach ($home['Image'] as $image): ?>
			<?php if($image['Elevation']['display']): ?>
			<li<?php echo $counter == 0 ? ' class="active"' : ''; ?>><a href="#img<?php echo $image['id']; ?>" data-toggle="tab"><?php echo $image['Elevation']['abbrev']; ?></a></li>
			<?php endif; ?>
			<?php $counter++; ?>
		<?php endforeach; ?>
		</ul>
	</div>
	<div class="downloads-top">
		<ul class="nav nav-pills">
			<?php foreach ($home['Attachment'] as $file): ?>
			<li><a href="<?php echo $this->Html->url('/files/attachments/' . $file['name']); ?>" class="btn btn-primary" target="_Blank"><?php echo __('Download PDF'); ?></a></li>
			<?php endforeach; ?>
			<li><a href="mailto:<?php echo $appConfig['email']; ?>?subject=<?php echo $home['Home']['name']; ?>&amp;body=I am interested in receiving further information regarding <?php echo $home['Home']['name']; ?>" class="btn btn-primary"><?php echo __('Request Info'); ?></a></li>
		</ul>
	</div>
</div>
<div class="row home-single">
	<div class="tab-content">
		<?php $counter = 0; ?>
		<?php foreach ($home['Image'] as $image): ?>
		<?php if($image['Elevation']['display']): ?>
			<div class="tab-pane<?php echo $counter == 0 ? ' active' : ''; ?>" id="img<?php echo $image['id']; ?>">
				<?php echo $this->Html->image('/img/homes/' . $image['name'], array('class' => 'img-responsive')); ?>
			</div>
		<?php endif; ?>
		<?php $counter++; ?>
		<?php endforeach; ?>
	</div>
	<div class="downloads-bottom">
		<ul class="nav nav-pills">
			<?php foreach ($home['Attachment'] as $file): ?>
			<li><a href="<?php echo $this->Html->url('/files/attachments/' . $file['name']); ?>" class="btn btn-primary btn-block" target="_blank"><?php echo __('Download PDF'); ?></a></li>
			<?php endforeach; ?>
			<li><a href="<?php echo $this->Html->url(array('controller' => 'page', 'action' => 'contact', $home['Community']['slug'])); ?>" class="btn btn-primary btn-block" target="_Blank"><?php echo __('Request Info'); ?></a></li>
		</ul>
	</div>
</div>
<hr class="fancy-hr">
<div class="row home-single">
	<h2 class="section-title"><?php echo __('Floorplans'); ?></h2>
	<div class="text-center">
		<?php foreach ($home['Image'] as $image): ?>
			<?php if($image['Elevation']['display'] == false): ?>
				<a href="<?php echo $this->Html->url('/img/homes/' . $image['name_alternative']); ?>" class="fancybox"><?php echo $this->Html->image('/thumbs/img/homes/0____400____' . $image['name']); ?></a>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
</div>
<div class="row home-single ctas">
	<div class="home-btn">
		<a href="<?php echo $this->Html->url(array('controller' => 'communities', 'action' => 'view', $home['Community']['slug'])); ?>#plans-and-pricing" class="btn btn-primary btn-block">View Another Model</a>
	</div>
	<div class="home-btn">
		<a href="<?php echo $this->Html->url(array('controller' => 'communities', 'action' => 'view', $home['Community']['slug'])); ?>" class="btn btn-primary btn-block">Back to Oxford Gate</a>
	</div>
</div>