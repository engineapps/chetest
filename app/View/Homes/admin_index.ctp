<div class="content">
	<h1><?php echo __('Homes'); ?></h1>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('Community.name', __('Community')); ?></th>
			<th><?php echo $this->Paginator->sort('Style.name', __('Style')); ?></th>
			<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($homes as $home): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $home['Home']['id']; ?>&nbsp;</td>
		<td><?php echo $home['Home']['name']; ?>&nbsp;</td>
		<td><?php echo $home['Community']['name']; ?>&nbsp;</td>
		<td><?php echo $home['Style']['name']; ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('Images'), array('controller' => 'images', $home['Home']['id']), array('class' => 'image')); ?>
			<?php echo $this->Html->link(__('Attachments'), array('controller' => 'attachments', $home['Home']['id']), array('class' => 'attachment')); ?>
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $home['Home']['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $home['Home']['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $home['Home']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $home['Home']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Home'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>