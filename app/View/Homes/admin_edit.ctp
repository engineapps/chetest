<div class="content">
	<?php echo $this->Form->create('Home', array('type' => 'file')); ?>
	<h1><?php echo __('Admin Edit Home'); ?></h1>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('slug');
		echo $this->Form->input('bedrooms');
		echo $this->Form->input('bathrooms');
		echo $this->Form->input('community_id');
		echo $this->Form->input('style_id');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $this->Form->value('Home.id')), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $this->Form->value('Home.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Homes'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>