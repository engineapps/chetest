<div class="content">
	<h1><?php echo __('Home'); ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $home['Home']['id']; ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $home['Home']['name']; ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Slug'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $home['Home']['slug']; ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Bedrooms'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $home['Home']['bedrooms']; ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Bathrooms'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $home['Home']['bathrooms']; ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Community'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $home['Community']['name']; ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Style'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $home['Style']['name']; ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Is Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $home['Home']['is_active']; ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($home['Home']['created']); ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($home['Home']['modified']); ?>
		</dd>
	</dl>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Home'), array('action' => 'edit', $home['Home']['id']), array('class' => 'edit')); ?></li>
		<li><?php echo $this->Html->link(__('Delete Home'), array('action' => 'delete', $home['Home']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $home['Home']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('List Homes'), array('action' => 'index'), array('class' => 'list')); ?></li>
		<li><?php echo $this->Html->link(__('New Home'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>
<div class="related">
	<h1><?php echo __('Related Images'); ?></h1>
	<?php if (!empty($home['Image'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Elevation'); ?></th>
		<th><?php echo __('Image'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($home['Image'] as $image): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $image['id']; ?></td>
		<td><?php echo $image['Elevation']['name']; ?></td>
		<td><?php echo $this->Html->image('/thumbs/img/homes/100____0____'.$image['name']); ?></td>
		<td><?php echo $this->Time->niceShort($image['created']); ?></td>
		<td class="text-right">
			<?php echo $this->Html->link(__('Edit'), array('controller' => 'images', 'action' => 'edit', $image['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('View'), array('controller' => 'images', 'action' => 'view', $image['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Delete'), array('controller' => 'images', 'action' => 'delete', $image['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $image['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php endif; ?>
</div>