<div class="content">
	<h1><?php echo __('Styles'); ?></h1>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($styles as $style): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $style['Style']['id']; ?>&nbsp;</td>
		<td><?php echo $style['Style']['name']; ?>&nbsp;</td>
		<td><?php echo $this->Time->niceShort($style['Style']['modified']); ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('1'), array('action' => 'moveup', $style['Style']['id'], 1), array('class' => 'up')); ?>
			<?php echo $this->Html->link(__('1'), array('action' => 'movedown', $style['Style']['id'], 1), array('class' => 'down')); ?>
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $style['Style']['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $style['Style']['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $style['Style']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $style['Style']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Style'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>