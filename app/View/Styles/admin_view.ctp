<div class="content">
	<h1><?php echo __('Style'); ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $style['Style']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $style['Style']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Is Published'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $style['Style']['is_published']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($style['Style']['created']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($style['Style']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Style'), array('action' => 'edit', $style['Style']['id']), array('class' => 'edit')); ?></li>
		<li><?php echo $this->Html->link(__('Delete Style'), array('action' => 'delete', $style['Style']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $style['Style']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('List Styles'), array('action' => 'index'), array('class' => 'list')); ?></li>
		<li><?php echo $this->Html->link(__('New Style'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>