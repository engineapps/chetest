<div class="content">
	<?php echo $this->Form->create('Style'); ?>
	<h1><?php echo __('Admin Edit Style'); ?></h1>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('parent_id', array('options' => $styles, 'empty' => '-- Parent Style --'));
		echo $this->Form->input('is_published');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Styles'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>