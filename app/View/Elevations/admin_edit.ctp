<div class="content">
	<?php echo $this->Form->create('Elevation'); ?>
	<h1><?php echo __('Admin Edit Elevation'); ?></h1>
	<fieldset>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('abbrev');
		echo $this->Form->input('display');
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $this->Form->value('Elevation.id')), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $this->Form->value('Elevation.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Elevations'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>