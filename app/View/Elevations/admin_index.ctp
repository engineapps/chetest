<div class="content">
	<h1><?php echo __('Elevations'); ?></h1>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('abbrev'); ?></th>
			<th><?php echo $this->Paginator->sort('display'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($elevations as $elevation): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $elevation['Elevation']['id']; ?>&nbsp;</td>
		<td><?php echo $elevation['Elevation']['name']; ?>&nbsp;</td>
		<td><?php echo $elevation['Elevation']['abbrev']; ?>&nbsp;</td>
		<td><?php echo $elevation['Elevation']['display']; ?>&nbsp;</td>
		<td><?php echo $this->Time->niceShort($elevation['Elevation']['modified']); ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $elevation['Elevation']['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $elevation['Elevation']['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $elevation['Elevation']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $elevation['Elevation']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Elevation'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>