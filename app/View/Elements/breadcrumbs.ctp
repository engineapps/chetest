<?php if(!empty($breadcrumbs)): ?>
<h1 class="breadcrumbs">
	<?php for($i=0; $i<count($breadcrumbs); $i++): ?>
	<?php if($i>0): ?> &raquo; <?php endif; ?>
	<?php echo !empty($breadcrumbs[$i][1]) ? $this->Html->link($breadcrumbs[$i][0], $breadcrumbs[$i][1]) : $breadcrumbs[$i][0]; ?>
	<?php endfor; ?>
</h1>
<?php endif; ?>