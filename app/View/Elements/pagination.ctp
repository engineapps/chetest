<div class="autoload">
	<ul class="pagination">
		<?php if($this->Paginator->hasPrev()) : ?>
		<li><?php echo $this->Paginator->prev('<< ' . __('previous'), array('class' => 'text', 'rel' => 'nofollow'), null, array('class' => 'disabled')); ?></li>
		<?php endif; ?>
		<?php if (is_string($this->Paginator->numbers())): ?>
		<li><?php echo str_replace('page:1"', 'page:1" rel="nofollow"', $this->Paginator->numbers(array('separator' => '</li><li>', 'tag' => 'span'))); ?></li>
		<?php endif; ?>
		<?php if($this->Paginator->hasNext()): ?>
		<li><?php echo $this->Paginator->next(__('next') . ' >>', array('class' => 'text', 'rel' => 'nofollow'), null, array('class' => 'disabled')); ?></li>
		<?php endif; ?>
		<li><span class="loading" style="display: none;"><?php echo __('loading'); ?></span></li>
	</ul>
	<br class="clear" />
</div>