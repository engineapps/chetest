<div class="row community">
	<h1 class="page-title"><?php echo __('Community Registration'); ?></h1>
	<?php if(!empty($community['Community']['formatted'])): ?>
	<div class="page-intro"><?php echo $community['Community']['formatted']; ?></div>
	<?php endif; ?>
	<?php if (!empty($community)): ?>
	<div id="community-headlines" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active"><?php echo $this->Html->image('communities/' . $community['Community']['logo'], array('class' => 'img-responsive')); ?></div>
		</div>
	</div>
	<?php endif; ?>
</div>
<hr class="fancy-hr">
<div class="row plans">
	<?php echo $this->Form->create('Registration', array('url' => array('action' => 'add'), 'id' => 'RegistrationForm')); ?>
	<?php echo $this->Session->flash(); ?>
	<fieldset class="checkboxes col-xs-12">
		<h3><?php echo __('Required Information') ?> &raquo;</h3>
		<?php echo $this->Form->input('Community', array('label' => __('Communities I am interested in:'), 'multiple' => 'checkbox', 'options' => $communities)); ?>
	</fieldset>
	<fieldset class="form clear">
		<br>
		<div class="col-xs-12"><h3><?php echo __('Personal Information') ?> &raquo;</h3></div>
		<div class="col-xs-12 col-sm-6 form-group"><?php echo $this->Form->input('first_name', array('class' => 'form-control')); ?></div>
		<div class="col-xs-12 col-sm-6 form-group"><?php echo $this->Form->input('last_name', array('class' => 'form-control')); ?></div>
		<div class="col-xs-12 col-sm-6 form-group"><?php echo $this->Form->input('email', array('class' => 'form-control')); ?></div>
		<div class="col-xs-12 col-sm-6 form-group"><?php echo $this->Form->input('phone', array('class' => 'form-control')); ?></div>
		<div class="col-xs-12 col-sm-6 form-group"><?php echo $this->Form->input('address', array('class' => 'form-control')); ?></div>
		<div class="col-xs-12 col-sm-6 form-group"><?php echo $this->Form->input('city', array('class' => 'form-control')); ?></div>
		<div class="col-xs-12 col-sm-6 form-group"><?php echo $this->Form->input('province', array('class' => 'form-control')); ?></div>
		<div class="col-xs-12 col-sm-6 form-group"><?php echo $this->Form->input('postal', array('class' => 'form-control')); ?></div>
	</fieldset>
	<fieldset class="checkboxes optional">
		<div class="col-xs-12"><h3 class="clear"><?php echo __('Optional Information') ?> &raquo;</h3></div>
		<div class="col-xs-12 col-md-6 form-group"><?php echo $this->Form->input('heard_from', array('class' => 'form-control', 'options' => $heardFrom, 'empty' => __('-- Select Source --'))); ?></div>
		<div class="col-xs-12 col-md-6 form-group">
			<label><?php echo __('Electronic Communications Consent'); ?></label>
			<?php echo $this->Form->input('is_subscribed', array('label' => __('I consent to receiving electronic communications from %s regarding upcoming communities, current communities, news, events, promotions and all other related communications. I am aware that I may withdraw my consent at any time by visiting', $appConfig['name']))); ?>
			<a href="<?php echo $this->Html->url('/casl', true); ?>"><?php echo $this->Html->url('/casl', true); ?></a>
		</div>
		<div class="col-xs-12 form-group"><?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?></div>
	</fieldset>
	<?php echo $this->Form->end(); ?>
</div>