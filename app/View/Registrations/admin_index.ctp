<div class="content">
	<h1><?php echo __('Registrations'); ?></h1>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('full_name'); ?></th>
		<th><?php echo $this->Paginator->sort('email'); ?></th>
		<th><?php echo $this->Paginator->sort('address'); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php $i = 0; ?>
	<?php foreach ($registrations as $registration): ?>
	<?php $class = ($i++ % 2 == 0) ? 'altrow' : null; ?>
	<tr class="<?php echo $class; ?>">
		<td><?php echo $registration['Registration']['id']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['full_name']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['email']; ?>&nbsp;</td>
		<td><?php echo $registration['Registration']['address']; ?>&nbsp;</td>
		<td class="text-right">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $registration['Registration']['id']), array('class' => 'edit')); ?>
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $registration['Registration']['id']), array('class' => 'view')); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $registration['Registration']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $registration['Registration']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Registration'), array('action' => 'add'), array('class' => 'add')); ?></li>
	</ul>
</div>