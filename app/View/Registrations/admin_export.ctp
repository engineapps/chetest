<?php
$fp = fopen('php://output', 'w');
$emails = array();
$header = array('id', 'first_name', 'last_name', 'email', 'phone', 'address', 'city', 'province', 'postal', 'heard_from', 'created', 'modified');
fputcsv($fp, $header);
foreach ($registrations as $registration) {
	$registration['Registration']['email'] = strtolower($registration['Registration']['email']);
	if (!empty($unique)) {
		if (in_array($registration['Registration']['email'], $emails)) {
			continue;
		}
		$emails[] = $registration['Registration']['email'];
	}
	$row = array(
		$registration['Registration']['id'],
		$registration['Registration']['first_name'],
		$registration['Registration']['last_name'],
		$registration['Registration']['email'],
		$registration['Registration']['phone'],
		$registration['Registration']['address'],
		$registration['Registration']['city'],
		$registration['Registration']['province'],
		$registration['Registration']['postal'],
		$registration['Registration']['heard_from'],
		$registration['Registration']['created'],
		$registration['Registration']['modified']
	);
	fputcsv($fp, $row);
}
fclose($fp);