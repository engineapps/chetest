<div class="content">
	<h1><?php echo __('Admin Add Registration'); ?></h1>
	<?php echo $this->Form->create('Registration'); ?>
	<fieldset>
		<?php
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('email');
		echo $this->Form->input('phone');
		echo $this->Form->input('address');
		echo $this->Form->input('city');
		echo $this->Form->input('province', array('options' => $provinces));
		echo $this->Form->input('postal');
		echo $this->Form->input('Community', array('multiple' => 'checkbox'));
		echo $this->Form->input('Style', array('multiple' => 'checkbox'));
		echo $this->Form->input('heard_from', array('options' => $heardFrom, 'empty' => __('-- Select Method --')));
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('List Registrations'), array('action' => 'index'), array('class' => 'list')); ?></li>
	</ul>
</div>