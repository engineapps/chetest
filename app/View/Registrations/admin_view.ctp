<div class="content">
	<h1><?php echo __('Registration'); ?></h1>
	<dl><?php $i = 0; $class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $registration['Registration']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('First Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $registration['Registration']['first_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Last Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $registration['Registration']['last_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Email'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $registration['Registration']['email']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Phone'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $registration['Registration']['phone']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Address'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $registration['Registration']['address']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('City'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $registration['Registration']['city']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Province'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $registration['Registration']['province']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Heard From'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $registration['Registration']['heard_from']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($registration['Registration']['created']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Time->niceShort($registration['Registration']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Registration'), array('action' => 'edit', $registration['Registration']['id']), array('class' => 'edit')); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Registration'), array('action' => 'delete', $registration['Registration']['id']), array('class' => 'delete'), __('Are you sure you want to delete # %s?', $registration['Registration']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Registrations'), array('action' => 'index'), array('class' => 'list')); ?> </li>
		<li><?php echo $this->Html->link(__('New Registration'), array('action' => 'add'), array('class' => 'add')); ?> </li>
	</ul>
</div>

<div class="related">
	<h1><?php echo __('Related Communities'); ?></h1>
	<?php if (!empty($registration['Community'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="text-right"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($registration['Community'] as $community):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class; ?>>
			<td><?php echo $community['id']; ?></td>
			<td><?php echo $community['name']; ?></td>
			<td><?php echo $this->Time->niceShort($community['created']); ?></td>
			<td><?php echo $this->Time->niceShort($community['modified']); ?></td>
			<td class="text-right">
				<?php echo $this->Html->link(__('View'), array('controller' => 'communities', 'action' => 'view', $community['id']), array('class' => 'view')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php endif; ?>
	
	<h1><?php echo __('Related Styles'); ?></h1>
	<?php if (!empty($registration['Style'])):?>
		<table cellpadding = "0" cellspacing = "0">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<th><?php echo __('Name'); ?></th>
			<th><?php echo __('Created'); ?></th>
			<th><?php echo __('Modified'); ?></th>
			<th class="text-right"><?php echo __('Actions'); ?></th>
		</tr>
		<?php
			$i = 0;
			foreach ($registration['Style'] as $style):
				$class = null;
				if ($i++ % 2 == 0) {
					$class = ' class="altrow"';
				}
			?>
			<tr<?php echo $class; ?>>
				<td><?php echo $style['id']; ?></td>
				<td><?php echo $style['name']; ?></td>
				<td><?php echo $this->Time->niceShort($style['created']); ?></td>
				<td><?php echo $this->Time->niceShort($style['modified']); ?></td>
				<td class="text-right">
					<?php echo $this->Html->link(__('View'), array('controller' => 'styles', 'action' => 'view', $style['id']), array('class' => 'view')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>
</div>