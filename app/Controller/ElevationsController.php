<?php

class ElevationsController extends AppController {

	public $name = 'Elevations';

	public function admin_index() {
		$this->Elevation->recursive = 0;
		$this->set('elevations', $this->paginate());
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid elevation'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('elevation', $this->Elevation->findById($id));
	}

	public function admin_add() {
		if (!$this->request->is('get')) {
			$this->Elevation->create();
			if ($this->Elevation->save($this->request->data)) {
				$this->Session->setFlash(__('The elevation has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The elevation could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid elevation'));
			$this->redirect(array('action' => 'index'));
		}
		if (!$this->request->is('get')) {
			if ($this->Elevation->save($this->request->data)) {
				$this->Session->setFlash(__('The elevation has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The elevation could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Elevation->findById($id);
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for elevation'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Elevation->delete($id)) {
			$this->Session->setFlash(__('Elevation deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Elevation was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}