<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array('Auth', 'Session', 'Upload');
	public $helpers = array('Session', 'Html', 'Form', 'Time', 'Minify.Minify');
	public $uses = array('User', 'Page', 'Menu');
	
	public function beforeFilter() {
		parent::beforeFilter();
		
		$this->appConfig = Configure::read('App');
		$this->set('appConfig', $this->appConfig);

		if (!empty($this->request->params['admin'])) {
			$this->layout = 'admin';
		}
		
		$this->breadcrumbs = array(0 => array(__('Home'), array('controller' => 'pages', 'action' => 'index')));
		if($this->request->params['action'] != 'admin_index' && $this->request->params['action'] != 'index') {
			$this->breadcrumbs[1] = array(Inflector::humanize($this->request->params['controller']), array('controller' => $this->request->params['controller'], 'action' => 'index'));
			$this->breadcrumbs[2] = array(Inflector::humanize($this->request->params['action']), null);
		} else {
			$this->breadcrumbs[1] = array(Inflector::humanize($this->request->params['controller']), null);
		}

		$nav = $this->Menu->getMenus();

		$this->set(compact('nav'));

		$this->Auth->authorize = array('Controller');
		$this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'login', 'admin' => true);
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login', 'admin' => true);
		$this->Auth->authenticate = array(
			'Form' => array('fields' => array('username' => 'email'), 'scope' => array('User.is_active' => true))
		);
	}
	
	public function beforeRender() {
		parent::beforeRender();
		
		if(!empty($this->viewVars['page'])) {
			if(empty($this->viewVars['slides'])) {
				$slides = $this->Page->Slide->getSlides($this->viewVars['page']['Page']['id']);
				$this->set(compact('slides'));
			}
			
			if(!empty($this->viewVars['page']['Page']['seo_title'])) {
				$this->set('title_for_layout', $this->appConfig['name'] . ' - ' . $this->viewVars['page']['Page']['seo_title']);
			} else if(!empty($this->viewVars['page']['Page']['title'])) {
				$this->set('title_for_layout', $this->appConfig['name'] . ' - ' . $this->viewVars['page']['Page']['title']);
			}
			if(!empty($this->viewVars['page']['Page']['seo_keywords'])) {
				$this->set('keywords_for_layout', $this->viewVars['page']['Page']['seo_keywords']);
			}
			if(!empty($this->viewVars['page']['Page']['seo_description'])) {
				$this->set('description_for_layout', $this->viewVars['page']['Page']['seo_description']);
			}
			if (!empty($this->viewVars['page']['Page']['contents'])) {
				$this->viewVars['page']['Page']['contents'] = __replaceVars($this->viewVars['page']['Page']['contents']);
			}
		}
	}
	
	public function isAuthorized($user = null) {
		if ($this->request->admin) {
			return (bool) $this->Auth->user('is_admin');
		} else {
			return (bool) $this->Auth->user('token');
		}

		return false;
	}

	/**
	 * Send out emails
	 * 
	 * @param type $data
	 * @return boolean
	 */
    protected function _sendEmail($data) {
        App::uses('CakeEmail', 'Network/Email');

        $data['Email']['layout'] = !empty($data['Email']['layout']) ? $data['Email']['layout'] : 'default';
        if (empty($data['Email']['fromName'])) {
            $data['Email']['fromName'] = $this->appConfig['name'];
        }

        $email = new CakeEmail('smtp');
        $email->to($data['Email']['to']);
        $email->subject($data['Email']['subject']);
        $email->template($data['Email']['template'], $data['Email']['layout']);
        $email->viewVars(array('data' => $data));
        $email->helpers(array('Html', 'Number'));

        if (!empty($data['Email']['bcc']) && is_array($data['Email']['bcc'])) {
            $email->bcc($data['Email']['bcc']);
        }

        if (!empty($data['Email']['attachments']) && is_array($data['Email']['attachments'])) {
            $email->attachments($data['Email']['attachments']);
        }
        !empty($data['Email']['from']) ? $email->from(array($data['Email']['from'] => $data['Email']['fromName'])) : 'no-reply@' . $_SERVER['SERVER_NAME'];
        !empty($data['Email']['replyTo']) ? $email->replyTo($data['Email']['replyTo']) : '';
        !empty($data['Email']['sendAs']) ? $email->emailFormat($data['Email']['sendAs']) : $email->emailFormat('html');

        try {
            if ($email->send()) {
                return true;
            }
        } catch (Exception $e) {
            try {
                $email->config('default');
                if ($email->send()) {
                    return true;
                }
            } catch (Exception $e) { }
        }

        return false;
    }

	protected function _getTweets($limit = 3) {
		if (Cache::read('tweets')) {
			$tweets = Cache::read('tweets');
		} else {
			App::import('Vendor', 'TwitterAPIExchange', array('file' => 'Twitter' . DS . 'TwitterAPIExchange.php'));
		
			$twitter = new TwitterAPIExchange($this->appConfig['Twitter']);
			$tweets = $twitter->getTweets($this->appConfig['Twitter']['username'], $limit);
			
			Cache::write('tweets', $tweets);
		}
		
		return $tweets;
	}
}
