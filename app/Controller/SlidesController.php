<?php

class SlidesController extends AppController {

	public $name = 'Slides';

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Upload->fileModel = 'Slide';
		$this->Upload->uploadDir = 'img/slides';
	}
	
	public function admin_index() {
		$this->Slide->recover();
		$this->Slide->recursive = 0;
		$this->set('slides', $this->paginate());
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid slide'));
			$this->redirect(array('action' => 'index'));
		}
		$slide = $this->Slide->findById($id);
		$this->set(compact('slide'));
	}

	public function admin_add() {
		if (!$this->request->is('get')) {
			$this->Slide->create();
			$this->request->data['Slide']['image'] = $this->Upload->upload('image');
			if ($this->Slide->save($this->request->data)) {
				$this->Session->setFlash(__('The slide has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The slide could not be saved. Please, try again.'));
			}
		}
		$this->set('pages', $this->Slide->Page->find('list'));
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid slide'));
			$this->redirect(array('action' => 'index'));
		}
		$slide = $this->Slide->findById($id);

		if (!$this->request->is('get')) {
			if (!$this->request->data['Slide']['image'] = $this->Upload->upload('image', $slide['Slide']['image'])) {
				unset($this->request->data['Slide']['image']);
			}
			if ($this->Slide->save($this->request->data)) {
				$this->Session->setFlash(__('The slide has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The slide could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $slide;
		}
		$this->set('pages', $this->Slide->Page->find('list'));
	}
	
	public function admin_moveup($id, $delta = null) {
		$slide = $this->Slide->findById($id);
		if (empty($slide)) {
			$this->Session->setFlash(__('Invalid slide'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Slide->id = $slide['Slide']['id'];

		if ($delta > 0) {
			$this->Slide->moveUp($this->Slide->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide a number of positions the slide should be moved up.'));
		}

		$this->redirect($this->referer());
	}

	public function admin_movedown($id, $delta = null) {
		$slide = $this->Slide->findById($id);
		if (empty($slide)) {
			$this->Session->setFlash(__('Invalid link'));
			$this->redirect(array('action' => 'index'), null, true);
		}

		$this->Slide->id = $slide['Slide']['id'];

		if ($delta > 0) {
			$this->Slide->moveDown($this->Slide->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide the number of positions the field should be moved down.'));
		}

		$this->redirect($this->referer());
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for slide'));
			$this->redirect(array('action' => 'index'));
		}
		$slide = $this->Slide->findById($id);

		if ($this->Slide->delete($id)) {
			if ($this->Upload->remove($slide['Slide']['image'])) {
				$this->Session->setFlash(__('Slide deleted'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			}
		}
		$this->Session->setFlash(__('Slide was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}