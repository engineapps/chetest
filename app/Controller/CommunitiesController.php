<?php

class CommunitiesController extends AppController {

	public $name = 'Communities';
	public $uses = array('Community', 'Image');

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Upload->fileModel = 'Community';
		$this->Upload->uploadDir = 'img/communities';
		
		$this->Auth->allow('index', 'view', 'admin_agent');
	}
	
	public function index($type = '') {
		$types = $this->Community->getTypes();
		$page = $this->Page->findBySlug($type);

		if (!$page) {
			$page = $this->Page->findBySlug('communities');
		}

		if (!empty($types[$type])) {
			$communities = $this->Community->find('all', array('conditions' => array('Community.type' => $type, 'Community.is_active' => '1', 'Community.is_sold' => '0')));
			$this->breadcrumbs[1][1] = array('controller' => 'communities');
			$this->breadcrumbs[2][0] = __('%s Communities', $types[$type]);
		} else {
			$communities = $this->Community->find('all', array('conditions' => array('Community.is_active' => '1')));
		}
		$communityTypes = array();
		foreach ($types as $key => $name) {
			$communityTypes[$key] = array();
		}
		foreach ($communities as $community) {
			$communityTypes[$community['Community']['type']][] = $community;
		}
		
		$this->set(compact('communities', 'page', 'type', 'types', 'communityTypes'));
	}
	
	public function view($slug) {
		$community = $this->Community->find('first', array('conditions' => array('Community.slug' => $slug)));
		if (!$community) {
			throw new NotFoundException();
		}
		$this->breadcrumbs[2][0] = $community['Community']['name'];

		$homes = $this->Community->Home->find('all', array(
				'conditions' => array(
						'Home.community_id' => $community['Community']['id']),
				'order' => 'Style.lft ASC, Home.sort_home_first ASC, Home.sort_home_last ASC',
				'contain' => array('Style', 'Image' => array('Elevation'))));
		
		// Pass seo data to page.
		$page['Page'] = $community['Community'];
		
		$this->set(compact('community', 'page', 'homes'));

		// Select view file
		if (file_exists(APP . 'View' . DS . 'Communities' . DS . 'view_' . $community['Community']['type'] . '.ctp')) {
			$this->render('view_' . $community['Community']['type']);
		}
	}

	public function admin_moveup($id, $delta = null) {
		$community = $this->Community->findById($id);
		if (empty($community)) {
			$this->Session->setFlash(__('Invalid page'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Community->id = $community['Community']['id'];

		if ($delta > 0) {
			$this->Community->moveUp($this->Community->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide a number of positions the field should be moved up.'));
		}

		$this->redirect(array('action' => 'index'), null, true);
	}

	public function admin_movedown($id, $delta = null) {
		$community = $this->Community->findById($id);
		if (empty($community)) {
			$this->Session->setFlash(__('Invalid page'));
			$this->redirect(array('action' => 'index'), null, true);
		}

		$this->Community->id = $community['Community']['id'];

		if ($delta > 0) {
			$this->Community->moveDown($this->Community->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide the number of positions the field should be moved down.'));
		}

		$this->redirect(array('action' => 'index'));
	}

	public function admin_index() {

		$communities = $this->paginate();
		$this->set(compact('communities'));
	}

	public function admin_agent() {
		if (!$this->Auth->loggedIn()) {
			$this->redirect(array('controller' => 'users', 'action' => 'login', 'admin' => true));
		}
		$communities = $this->paginate();

		//$page = $this->Page->findBySlug('communities');

		$this->set(compact('communities'));
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid community'));
			$this->redirect(array('action' => 'index'));
		}

		$this->set('community',
				$this->Community->find(
						'first',
						array(
								'conditions' => array(
										'Community.id' => $id
								),
								'contain' => array('Home')
						)
				)
		);
	}

	public function admin_add() {
		if (!$this->request->is('get')) {
			$this->Community->create();
			$this->request->data['Community']['logo'] = $this->Upload->upload('logo');
			$this->request->data['Community']['siteplan'] = $this->Upload->upload('siteplan');
			if ($this->Community->save($this->request->data)) {
				$this->Session->setFlash(__('The community has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The community could not be saved. Please, try again.'));
			}
		}
		$this->set('types', $this->Community->getTypes());
		$this->set('grids', $this->Community->getGrids());
		$this->set('offices', $this->Community->Office->find('list'));
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid community'));
			$this->redirect(array('action' => 'index'));
		}
		$community = $this->Community->findById($id);
		if (!$this->request->is('get')) {
			if (!$this->request->data['Community']['logo'] = $this->Upload->upload('logo', $community['Community']['logo'])) {
				unset($this->request->data['Community']['logo']);
			}
			if (!$this->request->data['Community']['siteplan'] = $this->Upload->upload('siteplan', $community['Community']['siteplan'])) {
				unset($this->request->data['Community']['siteplan']);
			}
			if ($this->Community->save($this->request->data)) {
				$this->Session->setFlash(__('The community has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The community could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $community;
		}
		$this->set('types', $this->Community->getTypes());
		$this->set('grids', $this->Community->getGrids());
		$this->set('offices', $this->Community->Office->find('list'));
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for community'));
			$this->redirect(array('action' => 'index'));
		}
		$community = $this->Community->findById($id);

		if ($this->Community->delete($id)) {
			$this->Upload->remove($community['Community']['logo']);
			$this->Session->setFlash(__('Community deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Community was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
