<?php

class AttachmentsController extends AppController {
	public $name = 'Attachments';
	public $uses = array('Attachment', 'Home');

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Upload->fileModel = 'Attachment';
		$this->Upload->uploadDir = 'files/attachments';
	}

	public function admin_index($home_id = null) {
		$conditions = (!empty($home_id)) ? array('Attachment.home_id' => $home_id) : null;
		$attachments = $this->paginate($conditions);
		$this->set(compact('attachments'));
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid attachment'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('attachment', $this->Attachment->findById($id));
	}

	public function admin_add($home_id = null) {
		if (!$this->request->is('get')) {
			$this->request->data['Attachment']['name'] = $this->Upload->upload('name');

			$this->Attachment->create();
			if ($this->Attachment->save($this->request->data)) {
				$this->Session->setFlash('Attachment has been saved.', 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Attachment']['home_id']));
			} else {
				$this->Session->setFlash(__('The attachment could not be saved. Please, try again.'));
			}
		}
		if ($home_id) {
			$this->request->data['Attachment']['home_id'] = $home_id;
		}

		$homes = $this->Attachment->Home->find('list');
		$this->set(compact('homes'));
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid attachment'));
			$this->redirect(array('action' => 'index'));
		}
		$attachment = $this->Attachment->findById($id);
		if (!$this->request->is('get')) {
			if (false == ($this->request->data['Attachment']['name'] = $this->Upload->upload('name', $attachment['Attachment']['name']))) {
				unset($this->request->data['Attachment']['name']);
			}

			if ($this->Attachment->save($this->request->data)) {
				$this->Session->setFlash(__('The image has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Attachment']['home_id']));
			} else {
				$this->Session->setFlash(__('The image could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Attachment->findById($id);
		}
		$homes = $this->Attachment->Home->find('list');
		$this->set(compact('homes'));
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for attachment'));
			$this->redirect(array('action' => 'index'));
		}
		$attachment = $this->Attachment->findById($id);

		if ($this->Attachment->delete($id)) {
			$this->Upload->remove($attachment['Attachment']['name']);
			$this->Session->setFlash('Attachment deleted', 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index', $attachment['Attachment']['home_id']));
		}
		$this->Session->setFlash(__('Attachment was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}