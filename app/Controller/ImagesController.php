<?php

class ImagesController extends AppController {

	public $name = 'Images';

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Upload->fileModel = 'Image';
		$this->Upload->uploadDir = 'img/homes';
	}

	public function admin_index($home_id = null) {
		if ($home_id) {
			$this->paginate = array('conditions' => array('Image.home_id' => $home_id));
		}

		$this->Image->recursive = 0;
		$this->set('images', $this->paginate());
		$this->set(compact('home_id'));
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid image'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('image', $this->Image->findById($id));
	}

	public function admin_add($home_id = null) {
		if (!$this->request->is('get')) {
			$this->request->data['Image']['name'] = $this->Upload->upload('name');
			$this->request->data['Image']['name_alternative'] = $this->Upload->upload('name_alternative');

			$this->Image->create();
			if ($this->Image->save($this->request->data)) {
				$this->Session->setFlash('Image has been saved.', 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Image']['home_id']));
			} else {
				$this->Session->setFlash(__('The image could not be saved. Please, try again.'));
			}
		}
		if ($home_id) {
			$this->request->data['Image']['home_id'] = $home_id;
		}

		$homes = $this->Image->Home->find('list');
		$elevations = $this->Image->Elevation->find('list');
		$this->set(compact('homes', 'elevations'));
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid image'));
			$this->redirect(array('action' => 'index'));
		}
		$image = $this->Image->findById($id);
		if (!$this->request->is('get')) {
			if (false == ($this->request->data['Image']['name'] = $this->Upload->upload('name', $image['Image']['name']))) {
				unset($this->request->data['Image']['name']);
			}
			if (false == ($this->request->data['Image']['name_alternative'] = $this->Upload->upload('name_alternative', $image['Image']['name_alternative']))) {
				unset($this->request->data['Image']['name_alternative']);
			}

			if ($this->Image->save($this->request->data)) {
				$this->Session->setFlash(__('The image has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Image']['home_id']));
			} else {
				$this->Session->setFlash(__('The image could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Image->findById($id);
		}
		$homes = $this->Image->Home->find('list');
		$elevations = $this->Image->Elevation->find('list');
		$this->set(compact('homes', 'elevations'));
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for image'));
			$this->redirect(array('action' => 'index'));
		}
		$image = $this->Image->findById($id);

		if ($this->Image->delete($id)) {
			$this->Upload->remove($image['Image']['name']);
			$this->Session->setFlash('Image deleted', 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index', $image['Image']['home_id']));
		}
		$this->Session->setFlash(__('Image was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}