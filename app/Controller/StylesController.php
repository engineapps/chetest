<?php

class StylesController extends AppController {

	public $name = 'Styles';

	public function admin_moveup($id, $delta = null) {
		$style = $this->Style->findById($id);
		if (empty($style)) {
			$this->Session->setFlash(__('Invalid page'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Style->id = $style['Style']['id'];

		if ($delta > 0) {
			$this->Style->moveUp($this->Style->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide a number of positions the field should be moved up.'));
		}

		$this->redirect(array('action' => 'index'), null, true);
	}

	public function admin_movedown($id, $delta = null) {
		$style = $this->Style->findById($id);
		if (empty($style)) {
			$this->Session->setFlash(__('Invalid page'));
			$this->redirect(array('action' => 'index'), null, true);
		}

		$this->Style->id = $style['Style']['id'];

		if ($delta > 0) {
			$this->Style->moveDown($this->Style->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide the number of positions the field should be moved down.'));
		}

		$this->redirect(array('action' => 'index'));
	}

	public function admin_index() {
		$this->Style->recursive = 0;
		$this->set('styles', $this->paginate());
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid style'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('style', $this->Style->findById($id));
	}

	public function admin_add() {
		if (!$this->request->is('get')) {
			$this->Style->create();
			if ($this->Style->save($this->request->data)) {
				$this->Session->setFlash(__('The style has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The style could not be saved. Please, try again.'));
			}
		}
		$styles = $this->Style->generateTreeList();
		$this->set(compact('styles'));
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid style'));
			$this->redirect(array('action' => 'index'));
		}
		if (!$this->request->is('get')) {
			if ($this->Style->save($this->request->data)) {
				$this->Session->setFlash(__('The style has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The style could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Style->findById($id);
		}
		$styles = $this->Style->generateTreeList();
		$this->set(compact('styles'));
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for style'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Style->delete($id)) {
			$this->Session->setFlash(__('Style deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Style was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
