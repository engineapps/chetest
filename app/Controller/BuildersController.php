<?php

class BuildersController extends AppController {

	public $name = 'Builders';

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Upload->fileModel = 'Builder';
		$this->Upload->uploadDir = 'img/builders';
	}

	public function admin_index() {
		$this->Builder->recursive = 0;
		$this->set('builders', $this->paginate());
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid builder'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('builder', $this->Builder->read(null, $id));
	}

	public function admin_add() {
		if (!$this->request->is('get')) {
			$this->Builder->create();
			$this->request->data['Builder']['image'] = $this->Upload->upload('image');
			if ($this->Builder->save($this->request->data)) {
				$this->Session->setFlash(__('The builder has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The builder could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid builder'));
			$this->redirect(array('action' => 'index'));
		}
		$builder = $this->Builder->findById($id);
		if (!$this->request->is('get')) {
			if (false == ($this->request->data['Builder']['image'] = $this->Upload->upload('image', $builder['Builder']['image']))) {
				unset($this->request->data['Builder']['image']);
			}
			if ($this->Builder->save($this->request->data)) {
				$this->Session->setFlash(__('The builder has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The builder could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $builder;
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for builder'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Builder->delete($id)) {
			$this->Session->setFlash(__('Builder deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Builder was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
