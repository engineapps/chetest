<?php

class UsersController extends AppController {

	public $name = 'Users';
	public $uses = array('User', 'Subscription');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('admin_login', 'admin_logout');

//		if($this->Auth->loggedIn()){
//			if($this->Auth->user('is_admin')){
//				$this->redirect(array('controller'=>'users', 'action' => 'index', 'admin' => true));
//			}
//			if($this->Auth->user('is_special')){
//				$this->redirect(array('controller'=>'communities', 'action' => 'agent', 'admin' => true));
//			}
//		}
	}

	public function admin_login() {

		if(($this->Auth->user('email') == "agent@voguegroup.com") && ($this->Auth->user('is_special'))){

			$this->redirect(array('controller'=>'communities', 'action' => 'agent', 'admin' => true));
		}

		if ($this->Auth->user('id')) {
			if(($this->Auth->user('is_admin')) || ($this->Auth->user('is_special'))){
				$this->redirect(array('action' => 'index', 'admin' => true));
			}else{
				$this->redirect(array('action' => 'index'));
			}

		}

		if (!$this->request->is('get')) {
			if ($this->Auth->login()) {
				$this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash(__('Username or password is incorrect'));
			}
		}
	}

	public function admin_logout() {
		$this->Session->destroy();
		$this->Auth->logout();
		$this->redirect(array('controller' => 'users', 'action' => 'login', 'admin' => true));
	}

	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	public function admin_export($type = 'all') {
		$this->User->recursive = -1;

		$users = $this->User->find('all', array('fields' => array(
				'id',
				'first_name',
				'last_name',
				'email',
				'is_subscribed'
		),
				'order' =>
						'id ASC'
		)
		);
		$headers = array(
			'User' => array(
				'id' => __('Id'),
				'first_name' => __('First Name'),
				'last_name' => __('Last Name'),
				'email' => __('Email'),
				'is_subscribed' => __('Subscription')
			)
		);

		$subscriptionUsers = $this->Subscription->find('all', array('fields' => array('id', 'email', 'is_subscribed'), 'order' => 'id ASC'));
		if ($type == 'newsletter') {
			$users = array();
		}
		if ($type !== 'user') {
			foreach ($subscriptionUsers as $user) {
				$users[] = array('User' => array(
						'id' => ' ',
						'first_name' => ' ',
						'last_name' => ' ',
						'email' => $user['Subscription']['email'],
						'subscription' => $user['Subscription']['is_subscribed']
					)
				);
			}
		}

		array_unshift($users, $headers);
		$this->set(compact('users'));

		header('Content-type: text/csv');
		header('Content-Disposition: attachment; filename="users.tsv"');
		foreach ($users as $user) {
			echo implode("\t", $user['User']) . "\n";
		}
		exit;
	}

	public function admin_view($id = null) {
		$user = $this->User->findById($id);
		if (!$user) {
			$this->Session->setFlash(__('Invalid user'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set(compact('user'));
	}

	public function admin_add() {
		if (!$this->request->is('get')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}

	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid user'));
			$this->redirect(array('action' => 'index'));
		}
		if (!$this->request->is('get')) {
			if (!empty($this->request->data['User']['new_password'])) {
				$this->request->data['User']['password'] = $this->request->data['User']['new_password'];
			}
			if ($this->User->save($this->request->data, array('validate' => false))) {
				$this->Session->setFlash(__('The user has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->User->findById($id);
		}
        if(isset($this->User->Group)){
            $this->set('groups', $this->User->Group->find('list'));
        }
	}

	public function admin_delete($id = null) {
		$user = $this->User->findById($id);
		if (!empty($user['Order'])) {
			$this->Session->setFlash(__('This user have associated orders, delete failed.'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('User deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}