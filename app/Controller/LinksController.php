<?php

class LinksController extends AppController {

	public $name = 'Links';

	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function admin_index($menu_id = null) {
		$conditions = (!empty($menu_id)) ? array('Link.menu_id' => $menu_id) : null;
		$links = $this->paginate($conditions);
		$this->set(compact('links', 'menu_id'));
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid link'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('link', $this->Link->findById($id));
	}

	public function admin_add($menu_id = null) {
		if (!$this->request->is('get')) {
			$this->Link->create();
			if ($this->Link->save($this->request->data)) {
				$this->Session->setFlash(__('Link has been saved.'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Link']['menu_id']));
			} else {
				$this->Session->setFlash(__('The link could not be saved. Please, try again.'));
			}
		}
		$links = $this->Link->generateTreeList(array('Link.menu_id' => $menu_id));
		$menus = $this->Link->Menu->find('list');
		$this->set(compact('links', 'menus', 'menu_id'));
	}

	public function admin_edit($id = null, $menu_id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid link'));
			$this->redirect(array('action' => 'index'));
		}
		if (!$this->request->is('get')) {
			if ($this->Link->save($this->request->data)) {
				$this->Session->setFlash(__('The link has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Link']['menu_id']));
			} else {
				$this->Session->setFlash(__('The link could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Link->findById($id);
		}
		if(!empty($menu_id)) {
			$this->request->data['Link']['menu_id'] = $menu_id;
		}
		$links = $this->Link->generateTreeList(array('Link.menu_id' => $this->request->data['Link']['menu_id']));
		$menus = $this->Link->Menu->find('list');
		$this->set(compact('links', 'menus'));
	}

	public function admin_moveup($id, $delta = null) {
		$link = $this->Link->findById($id);
		if (empty($link)) {
			$this->Session->setFlash(__('Invalid link'));
			$this->redirect(array('action' => 'index'));
		}

		$prev = $this->Link->find('all', array('conditions' => array('Link.parent_id' => $link['Link']['parent_id'], 'Link.lft <' => $link['Link']['lft']), 'order' => 'Link.lft DESC'));
		if (!empty($prev) && $delta <= 1) {
			$delta = 1;
			foreach($prev as $item) {
				if ($item['Menu']['id'] == $link['Menu']['id']) {
					break;
				} else {
					$delta++;
				}
			}
		}
		
		$this->Link->id = $link['Link']['id'];

		if ($delta > 0) {
			$this->Link->moveUp($this->Link->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide a number of positions the link should be moved up.'));
		}

		$this->redirect($this->referer());
	}

	public function admin_movedown($id, $delta = null) {
		$link = $this->Link->findById($id);
		if (empty($link)) {
			$this->Session->setFlash(__('Invalid link'));
			$this->redirect(array('action' => 'index'), null, true);
		}
		
		$next = $this->Link->find('all', array('conditions' => array('Link.parent_id' => $link['Link']['parent_id'], 'Link.lft >' => $link['Link']['lft'])));
		if (!empty($next) && $delta <= 1) {
			$delta = 1;
			foreach($next as $item) {
				if ($item['Menu']['id'] == $link['Menu']['id']) {
					break;
				} else {
					$delta++;
				}
			}
		}

		$this->Link->id = $link['Link']['id'];

		if ($delta > 0) {
			$this->Link->moveDown($this->Link->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide the number of positions the field should be moved down.'));
		}

		$this->redirect($this->referer());
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for link'));
			$this->redirect(array('action' => 'index'));
		}
		$link = $this->Link->findById($id);

		if ($this->Link->delete($id)) {
			$this->Session->setFlash(__('Link deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index', $link['Menu']['id']));
		}
		$this->Session->setFlash(__('Link was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
