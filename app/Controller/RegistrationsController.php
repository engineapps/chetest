<?php

class RegistrationsController extends AppController {

	var $name = 'Registrations';
	var $uses = array('Registration', 'Subscription');

	function beforeFilter() {
		parent::beforeFilter();
		
		$this->Auth->allow('add', 'index','admin_export');
	}
	
	function index() {
		$this->redirect(array('action' => 'add'));
	}
	
	function add($slug = null) {
		$community = $this->Registration->CommunityRegistration->Community->find('first', array('conditions' => array('Community.slug' => $slug, 'Community.is_upcoming' => '1')));
		if ($community) {
			$this->request->data['Community']['Community'][] = $community['Community']['id'];
			$this->breadcrumbs[2][0] = $community['Community']['name'];
			$this->set('title_for_layout', __('%s Registration', $community['Community']['name']));
		} else if($slug && !$community) {
			$this->Session->setFlash(__('This community is no longer accepting registrations.'));
			$this->redirect(array('action' => 'add'));
		}

		if (!$this->request->is('get')) {
			$this->Registration->create();
			$this->request->data['CommunityRegistration'] = array();
			if(!empty($this->request->data['Registration']['Community'])) {
				foreach($this->request->data['Registration']['Community'] as $i => $community_id) {
					$this->request->data['CommunityRegistration'][$i]['community_id'] = $community_id;
				}
				unset($this->request->data['Registration']['Community']);
			}
			if ($this->Registration->saveAll($this->request->data)) {
				$data = $this->request->data;
				$data['Data']['communities'] = $this->Registration->CommunityRegistration->Community->find('list', array('conditions' => array('Community.id' => $data['Community']['Community'])));

				if (!empty($data['Registration']['is_subscribed'])) {
					$subscription = array('email' => $data['Registration']['email'], 'is_subscribed' => $data['Registration']['is_subscribed'], 'ip' => $this->request->clientIp());
					$this->Subscription->create();
					$this->Subscription->save($subscription);
				}

				$data['Data']['name'] = $this->appConfig['name'];
				$data['Email']['to'] = $data['Registration']['email'];
				$data['Email']['from'] = $this->appConfig['email'];
				$data['Email']['replyTo'] = $this->appConfig['email'];
				$data['Email']['template'] = 'registration';
				$data['Email']['subject'] = __('Thank you for registration with %s', $this->appConfig['name']);
				$this->_sendEmail($data);

                // Custom Notification
                $data['Email']['to'] = $this->appConfig['emailNotify'];
                $data['Email']['subject'] = __('Registration Notification - %s', $this->appConfig['name']);
                $data['Email']['template'] = 'registration_notification';
                $this->_sendEmail($data);

				$this->Session->setFlash(__('You have been successfully registered with us. Thank you!'), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('Registration not saved. Please fix the highlighted field below.'));
			}
		}
		$provinces = $this->Registration->getProvinces();
		$heardFrom = $this->Registration->getHeardFrom();
		$styles = $this->Registration->Style->find('list', array('conditions' => array('Style.is_published' => '1')));
		$communities = $this->Registration->CommunityRegistration->Community->find('list', array('conditions' => array('Community.is_upcoming' => '1')));
		$this->set('slug', 'registration');
		$this->set(compact('styles', 'communities', 'provinces', 'heardFrom', 'contactMethods', 'community'));
	}

	function admin_index($community_id = null) {
		if ($community_id) {
			$this->paginate = array('conditions' => array('CommunityRegistration.community_id' => $community_id));
		}
		
		$registrations = $this->paginate();

		$this->set(compact('registrations'));
	}

	function admin_export($community_id) {
    
		$this->Registration->Community->recursive = -1;
		$community = $this->Registration->Community->findById($community_id);
        
        
        
		if (empty($community)) {
			$this->Session->setFlash(__('Invalid community'));
			$this->redirect(array('action' => 'index'));
		}

		$registrations = $this->Registration->CommunityRegistration->find('all', 
			array(
					'conditions' => array('CommunityRegistration.community_id' => $community_id), 
					'contain' => array('Registration'), 
					'order' => 'Registration.created ASC',
					'group' => 'Registration.email'
				)
			);
      //  debug($registrations); exit();
		$this->set(compact('registrations', 'community'));
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"registrations-".$community['Community']['slug'].'-'.date('Y-m-d').".csv");
		$this->layout = 'ajax';
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid registration'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('registration', $this->Registration->findById($id));
	}

	function admin_add($community_id = null) {
		if ($community_id) {
			$this->request->data['Registration']['community_id'] = $community_id;
		}

		if (!$this->request->is('get')) {
			$this->Registration->create();
			if ($this->Registration->save($this->request->data)) {
				$this->Session->setFlash('Registration has been saved.', 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The registration could not be saved. Please, try again.'));
			}
		}
		$provinces = $this->Registration->getProvinces();
		$heardFrom = $this->Registration->getHeardFrom();
		$styles = $this->Registration->Style->find('list');
		$communities = $this->Registration->Community->find('list');
		$this->set(compact('styles', 'communities', 'provinces', 'heardFrom'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid registration'));
			$this->redirect(array('action' => 'index'));
		}
		if (!$this->request->is('get')) {
			if ($this->Registration->save($this->request->data)) {
				$this->Session->setFlash(__('The registration has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The registration could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Registration->findById($id);
		}
		$provinces = $this->Registration->getProvinces();
		$heardFrom = $this->Registration->getHeardFrom();
		$styles = $this->Registration->Style->find('list');
		$communities = $this->Registration->Community->find('list');
		$this->set(compact('styles', 'communities', 'provinces', 'heardFrom'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for registration'));
			$this->redirect(array('action' => 'index'));
		}

		if ($this->Registration->delete($id)) {
			$this->Session->setFlash('Registration deleted', 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Registration was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
