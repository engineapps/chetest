<?php

class OfficesController extends AppController {

	public $name = 'Offices';

	public function beforeFilter() {
		parent::beforeFilter();
		
		$this->Upload->fileModel = 'Office';
		$this->Upload->uploadDir = 'img/offices';
	}
	
	public function admin_index() {
		$this->Office->recursive = 0;
		$this->set('offices', $this->paginate());
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid office'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('office', $this->Office->findById($id));
	}

	public function admin_add() {
		if (!$this->request->is('get')) {
			$this->Office->create();
			$this->request->data['Office']['map'] = $this->Upload->upload('map');
			if ($this->Office->save($this->request->data)) {
				$this->Session->setFlash(__('The office has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The office could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid office'));
			$this->redirect(array('action' => 'index'));
		}
		$office = $this->Office->findById($id);
		if (!$this->request->is('get')) {
			if (!$this->request->data['Office']['map'] = $this->Upload->upload('map', $office['Office']['map'])) {
				unset($this->request->data['Office']['map']);
			}
			if ($this->Office->save($this->request->data)) {
				$this->Session->setFlash(__('The office has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The office could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $office;
		}
	}
	
	public function admin_moveup($id, $delta = null) {
		$office = $this->Office->findById($id);
		if (empty($office)) {
			$this->Session->setFlash(__('Invalid office'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Office->id = $office['Office']['id'];

		if ($delta > 0) {
			$this->Office->moveUp($this->Office->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide a number of positions the field should be moved up.'));
		}

		$this->redirect($this->referer());
	}

	public function admin_movedown($id, $delta = null) {
		$office = $this->Office->findById($id);
		if (empty($office)) {
			$this->Session->setFlash(__('Invalid office'));
			$this->redirect(array('action' => 'index'), null, true);
		}

		$this->Office->id = $office['Office']['id'];

		if ($delta > 0) {
			$this->Office->moveDown($this->Office->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide the number of positions the field should be moved down.'));
		}

		$this->redirect($this->referer());
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for office'));
			$this->redirect(array('action' => 'index'));
		}
		$office = $this->Office->findById($id);

		if ($this->Office->delete($id)) {
			$this->Upload->remove($office['Office']['map']);
			$this->Session->setFlash(__('Office deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Office was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
