<?php

class MenusController extends AppController {

	public $name = 'Menus';

	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function admin_index($page_id = null) {
		$this->Menu->recursive = 0;
		$this->set('menus', $this->paginate());
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid menu'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('menu', $this->Menu->findById($id));
	}

	public function admin_add($page_id = null) {
		if ($page_id) {
			$this->request->data['Menu']['page_id'] = $page_id;
		}

		if (!$this->request->is('get')) {
			$this->Menu->create();
			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('Menu has been saved.'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Menu']['page_id']));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid menu'));
			$this->redirect(array('action' => 'index'));
		}
		if (!$this->request->is('get')) {
			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('The menu has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Menu']['page_id']));
			} else {
				$this->Session->setFlash(__('The menu could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Menu->findById($id);
		}
	}


	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for menu'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Menu->delete($id)) {
			$this->Menu->Link->deleteAll(array('Link.menu_id' => $id));
			$this->Session->setFlash(__('Menu deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Menu was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
