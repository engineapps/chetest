<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */

class PagesController extends AppController {

	public $name = 'Pages';
	public $uses = array('Page', 'Contact');

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow('index', 'contact', 'thumbs', 'zoomify');

		$this->Upload->fileModel = 'Page';
		$this->Upload->uploadDir = 'img/pages';
	}

	public function index($slug = 'home') {
		$page = $this->Page->findBySlug($slug);

		if (!$page) {
			throw new NotFoundException();
		}

		$slides = $this->Page->Slide->getSlides($page['Page']['id']);

		$this->set(compact('page', 'slides'));

		if (file_exists(APP . 'View' . DS . ucfirst($this->request->controller) . DS . $page['Page']['slug'] . '.ctp')) {
			$this->render($page['Page']['slug']);
		}
	}

	public function contact() {
		if (!$this->request->is('get')) {
			if ($this->Contact->save($this->request->data)) {
				$data['Contact'] = $this->request->data['Contact'];
				$data['Email']['to'] = $this->appConfig['emailNotify'];
				$data['Email']['from'] = $this->request->data['Contact']['email'];
				$data['Email']['fromName'] = $this->request->data['Contact']['name'];
				$data['Email']['replyTo'] = $this->request->data['Contact']['email'];
				$data['Email']['template'] = 'contact_message';
				$data['Email']['subject'] = __('Contact inquiry from %s', $this->appConfig['name']);
				if ($this->_sendEmail($data)) {
					$this->Session->setFlash(__('Thanks for contacting us. We will get back to you asap.'), 'default', array('class' => 'success'));
				} else {
					$this->Session->setFlash(__('Error submitting message. Please try again later.'));
				}
				
				$this->redirect(array('action' => 'contact'));
			} else {
				$this->Session->setFlash(__('Error submitting message.'));
			}
		}
		$page = $this->Page->findBySlug('contact');

		$this->set(compact('page'));
	}

	public function thumbs() {
		App::import('Vendor', 'phpthumb', array('file' => 'phpThumb' . DS . 'phpthumb.class.php'));
		$phpThumb = new phpThumb();

		$this->layout = null;
		$this->autoRender = false;

		$cacheFilename = end($this->request->params['pass']);
		preg_match_all('/^([0-9]*)\_{4}([0-9]*)\_{4}(.*)$/', $cacheFilename, $matches);
		if (empty($matches[3][0])) {
			header('Cache-Control: no-cache');
			header('Content-type: image/gif');
			header('Content-length: 43');

			echo base64_decode('R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==');
			exit;
		}
		array_pop($this->request->params['pass']);

		$sourceDirectory = WWW_ROOT . implode('/', $this->request->params['pass']);
		$sourceFilename = $sourceDirectory . DS . $matches[3][0];
		$cacheDirectory = WWW_ROOT . 'thumbs' . DS . implode('/', $this->request->params['pass']);

		$allowedParameters = array('src', 'new', 'w', 'h', 'wp', 'hp', 'wl', 'hl', 'ws', 'hs', 'f', 'q', 'sx', 'sy', 'sw', 'sh', 'zc', 'bc', 'bg', 'bgt', 'fltr', 'xto', 'ra', 'ar', 'aoe', 'far', 'iar', 'maxb', 'down', 'phpThumbDebug', 'hash', 'md5s', 'sfn', 'dpi', 'sia', 'nocache');

		if ($matches[1][0] > 0) {
			$phpThumb->setParameter('w', $matches[1][0]);
		}
		if ($matches[2][0] > 0) {
			$phpThumb->setParameter('h', $matches[2][0]);
		}
		if (!empty($matches[3][0])) {
			$phpThumb->setParameter('src', $sourceDirectory . DS . $matches[3][0]);
			$split = explode('.', $matches[3][0]);
			$ext = end($split);
			if (!empty($ext)) {
				$phpThumb->setParameter('f', $ext);
			}
		}

		App::uses('Folder', 'Utility');
		$folder = new Folder(WWW_ROOT . 'thumbs');
		if (!file_exists($cacheDirectory)) {
			$folder->create($cacheDirectory, 0777);
		}

		$phpThumb->config_imagemagick_path = '/usr/bin/convert';
		$phpThumb->config_prefer_imagemagick = true;
		$phpThumb->config_error_die_on_error = true;
		$phpThumb->config_document_root = '';
		$phpThumb->config_temp_directory = APP . 'tmp';
		$phpThumb->config_cache_directory = $cacheDirectory;
		$phpThumb->config_cache_disable_warning = false;
		$phpThumb->cache_filename = $phpThumb->config_cache_directory . DS . $cacheFilename;
		if (!is_file($phpThumb->cache_filename)) {
			if ($phpThumb->GenerateThumbnail()) {
				$phpThumb->RenderToFile($phpThumb->cache_filename);
			} else {
				$phpThumb->ErrorImage('Failure: ' . $phpThumb->error);
				exit;
			}
		}

		$modifiedCache = filemtime($phpThumb->cache_filename);
		$modifiedSource = filemtime($sourceFilename);
		if ($modifiedSource > $modifiedCache) {
			@unlink($phpThumb->cache_filename);
			if ($phpThumb->GenerateThumbnail()) {
				$phpThumb->RenderToFile($phpThumb->cache_filename);
			} else {
				$phpThumb->ErrorImage('Failure: ' . $phpThumb->error);
				exit;
			}
		}
		if (headers_sent()) {
			$phpThumb->ErrorImage('Headers already sent (' . basename(__FILE__) . ' line ' . __LINE__ . ')');
			exit;
		}
		header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
		if (($cachedImage = getimagesize($phpThumb->cache_filename)) != false) {
			header('Content-Type: ' . $cachedImage['mime']);
		} elseif (eregi('\.ico$', $phpThumb->cache_filename)) {
			header('Content-Type: image/x-icon');
		}
		readfile($phpThumb->cache_filename);
		exit;
	}
	
	public function zoomify() {
		$this->layout = null;
		$this->autoRender = false;

		$cacheFilename = end($this->request->params['pass']);
		array_pop($this->request->params['pass']);
		$sourceFilename = WWW_ROOT . implode(DS, $this->request->params['pass']) . DS . $cacheFilename . '.jpg';
		$cacheDirectory = WWW_ROOT . 'zoomify' . DS . implode(DS, $this->request->params['pass']);
		$cacheFullPath = $cacheDirectory . DS . $cacheFilename;

		App::import("Vendor", "zoomifyImage/contrib/php_wrapper/zoomify");

		// Create parent directory, if it doesn't exists
		App::uses('Folder', 'Utility');
		$folder = new Folder(WWW_ROOT . 'zoomify');
		if (!file_exists($cacheDirectory)) {
			$folder->create($cacheDirectory, 0777);
		}
		
		$zoomify = new zoomify($cacheFullPath . DS);
		$zoomify->zoomifyObject($sourceFilename, $cacheFullPath);

		exit;
	}

	public function admin_index() {
		$this->Page->recursive = 0;
		$this->set('pages', $this->paginate());
	}

	public function admin_purge() {
		Cache::clear(false, 'default');
		Cache::clear(false, 'short');

		App::uses('Folder', 'Utility');
		$tmpDir = APP . 'tmp' . DS . 'cache' . DS . 'minify' . DS;
		$tmpFolder = new Folder($tmpDir);
		$tmpFiles = $tmpFolder->find('minify.+');
		if (!empty($tmpFiles) && is_array($tmpFiles)) {
			foreach ($tmpFiles as $tmpFile) {
				unlink($tmpDir . $tmpFile);
			}
		}
		
		$this->Session->setFlash(__('All cache is cleared.'), 'default', array('class' => 'success'));
		$this->redirect($this->referer());
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid page'));
			$this->redirect(array('action' => 'index'));
		}
		$page = $this->Page->findById($id);
		$this->set(compact('page'));
	}

	public function admin_add() {
		if (!$this->request->is('get')) {
			$this->Page->create();
			$this->request->data['Page']['banner'] = $this->Upload->upload('banner');
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(__('The page has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid page'));
			$this->redirect(array('action' => 'index'));
		}
		$page = $this->Page->findById($id);

		if (!$this->request->is('get')) {
			if (!$this->request->data['Page']['banner'] = $this->Upload->upload('banner', $page['Page']['banner'])) {
				unset($this->request->data['Page']['banner']);
			}
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(__('The page has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The page could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $page;
		}
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for page'));
			$this->redirect(array('action' => 'index'));
		}
		$page = $this->Page->findById($id);

		if ($this->Page->delete($id)) {
			$this->Upload->remove($page['Page']['image']);
			$this->Session->setFlash(__('Page deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Page was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function admin_images() {
		header('Content-Type: application/json');
		
		$images = $this->Page->readDirectory('img/media');
		echo json_encode($images); exit;
	}
	
	public function admin_upload() {
		header('Content-Type: application/json');
		
		$this->Upload->fileModel = 'Page';
		$this->Upload->uploadDir = 'img/media';
		if (!empty($this->request->form['file'])) {
			$this->Upload->requestData[$this->Upload->fileModel]['file'] = $this->request->form['file'];
			$uploadedFile = $this->Upload->upload('file');
		}
		$array = array(
			'filelink' => Router::url(DS . $this->Upload->uploadDir . DS . $uploadedFile)
		);
		echo stripslashes(json_encode($array)); exit;
	}

	public function admin_home() {

		$graph = $this->Page->getAnalytics();

		$models = $this->Page->getSearchModels();
		$results = array();
		$q = '';
		if (!empty($this->request->query['q'])) {
			$q = trim($this->request->query['q']);

			foreach ($models as $model => $fields) {
				App::import('Model', $model);
				$instance = new $model();
				$conditions = array();
				foreach ($fields as $field) {
					$conditions["OR"]["$field LIKE"] = "%$q%";
				}
				$results[$model] = $instance->find('all', array('conditions' => $conditions, 'recursive' => '-1'));
			}
		}
		$this->set(compact('q', 'results', 'models', 'graph'));
	}

}