<?php

class SubscriptionsController extends AppController {

	public $name = 'Subscriptions';
	public $uses = array('Subscription');

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow(array('index', 'consent', 'no_consent', 'subscribe', 'authorize'));
		
		$page = $this->Page->findBySlug('subscriptions');
		$this->set(compact('page'));
	}

	public function index() {
		if (!$this->request->is('get')) {
			$subscription = $this->Subscription->find('first', array('conditions' => array('email' => $this->request->data['Subscription']['email'], 'email !=' => '')));
			if (!empty($subscription['Subscription']['id'])) {
				// Send out confirmation link
				$this->request->data['Subscription']['id'] = $subscription['Subscription']['id'];
				$this->request->data['Subscription']['ip'] = $this->request->clientIp();
				if ($this->Subscription->save($this->request->data)) {
					$this->redirect(array('action' => 'authorize', $this->request->data['Subscription']['email']));
				} else {
					$this->Session->setFlash(__('Please fix errors below.'));
				}
			} else {
				// You are not subscribed
				$this->Session->setFlash(__('You are not subscribed to our mailing list.'));
			}
		}
		
		if (!empty($subscription['Subscription']) && empty($this->request->data)) {
			$this->request->data['Subscription'] = $subscription['Subscription'];
		}
	}
	
	public function consent($hash = '') {
		$subscription = $this->Subscription->findByHash($hash);
		
		if (empty($hash) || empty($subscription)) {
			$this->redirect(array('action' => 'index'));
		}
		
		if (!$this->request->is('get')) {
			$this->Subscription->id = $subscription['Subscription']['id'];
			$this->request->data['Subscription']['ip'] = $this->request->clientIp();
			$this->request->data['Subscription']['is_subscribed'] = '1';
			$this->request->data['Subscription']['is_revoked'] = '0';
			if ($subscription['Subscription']['email'] != $this->request->data['Subscription']['email']) {
				$this->request->data['Subscription']['email_old'] = $subscription['Subscription']['email'];
				$this->request->data['Subscription']['is_subscribed'] = '0';
			}
			$this->Subscription->validator()->remove('is_subscribed');
			if ($this->Subscription->save($this->request->data)) {
				if (empty($this->request->data['Subscription']['is_subscribed'])) {
					$data = array('appConfig' => $this->appConfig, 'Subscription' => $subscription['Subscription']);
					$data['Email']['to'] = $this->request->data['Subscription']['email'];
					$data['Email']['from'] = $this->appConfig['email'];
					$data['Email']['fromName'] = $this->appConfig['name'];
					$data['Email']['replyTo'] = $this->appConfig['email'];
					$data['Email']['template'] = 'subscription_authorize';
					$data['Email']['subject'] = __('%s - Please verify your consent', $this->appConfig['name']);

					if ($this->_sendEmail($data)) {
						$this->Session->setFlash(__('We have sent you an email to confirm your consent. Please follow the instructions within that email to complete the process.'), 'default', array('class' => 'info'));
					}
				} else {
					$this->redirect(array('action' => 'subscribe', $hash, '1'));
				}
			} else {
				$this->Session->setFlash(__('Please fix errors below.'));
			}
		}
		
		if (!empty($subscription) && empty($this->request->data)) {
			$this->request->data = $subscription;
		}
	}

	public function no_consent($hash = '') {
		$subscription = $this->Subscription->findByHash($hash);
		
		if (empty($hash) || empty($subscription)) {
			$this->redirect(array('action' => 'index'));
		}
		
		if (!$this->request->is('get')) {
			$this->Subscription->id = $subscription['Subscription']['id'];
			$this->request->data['Subscription']['ip'] = $this->request->clientIp();
			$this->request->data['Subscription']['is_subscribed'] = '0';
			$this->request->data['Subscription']['is_revoked'] = '1';
			$this->Subscription->validator()->remove('is_subscribed');
			if ($this->Subscription->save($this->request->data)) {
				$this->redirect(array('action' => 'subscribe', $hash, '0'));
			}
		}
		
		if (!empty($subscription) && empty($this->request->data)) {
			$this->request->data = $subscription;
		}
	}
	
	public function subscribe($hash, $status) {
		$subscription = $this->Subscription->findByHash($hash);
		if (!empty($subscription)) {
			$this->Subscription->id = $subscription['Subscription']['id'];
			if (!empty($status)) {
				$this->Subscription->saveField('is_subscribed', '1');
				$this->Subscription->saveField('is_revoked', '0');
			} else {
				$this->Subscription->saveField('is_subscribed', '0');
				$this->Subscription->saveField('is_revoked', '1');
			}
			$subscription = $this->Subscription->findByHash($hash);
		}
		
		$this->set(compact('subscription'));
	}

	public function authorize($email) {
		$subscription = $this->Subscription->find('first', array('conditions' => array('email' => $email)));
		if (!empty($subscription)) {
			$subscription['Subscription']['hash'] = md5($subscription['Subscription']['hash'] . time());
			$data = array('appConfig' => $this->appConfig, 'Subscription' => $subscription['Subscription']);
			$data['Email']['to'] = $email;
			$data['Email']['from'] = $this->appConfig['email'];
			$data['Email']['fromName'] = $this->appConfig['name'];
			$data['Email']['replyTo'] = $this->appConfig['email'];
			$data['Email']['template'] = 'subscription_revoke';
			$data['Email']['subject'] = __('%s - Consent Withdrawal Verification Email', $this->appConfig['name']);

			$this->Subscription->id = $subscription['Subscription']['id'];
			$this->Subscription->saveField('hash', $subscription['Subscription']['hash']);

			if ($this->_sendEmail($data)) {
				$this->Session->setFlash(__('Email with verification link has been sent to your email, Follow the link in the email to continue.'), 'default', array('class' => 'info'));
				$this->redirect(array('action' => 'index'));
			}
		}
		exit;
	}

	public function admin_index() {
		$this->Subscription->recursive = 0;
		$this->set('subscriptions', $this->paginate());
	}

	public function admin_export() {
		$subscriptions = $this->Subscription->find('all', array('conditions' => array('Subscription.is_subscribed' => '1'), 'group' => 'Subscription.id', 'order' => 'Subscription.created ASC'));
		$this->set(compact('subscriptions'));

		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"subscriptions.csv\"");
		$this->layout = 'ajax';
	}

	public function admin_view($id = null) {
		$subscription = $this->Subscription->find('first', array('conditions' => array('Subscription.id' => $id), 'contain' => array('CommunitySubscription' => array('Community'))));
		if (!$subscription) {
			$this->Session->setFlash(__('Invalid subscription'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set(compact('subscription'));
	}

	public function admin_add() {
		if (!$this->request->is('get')) {
			if ($this->Subscription->save($this->request->data)) {
				$this->Session->setFlash(__('The subscription has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The subscription could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid subscription'));
			$this->redirect(array('action' => 'index'));
		}
		if (!$this->request->is('get')) {
			if ($this->Subscription->save($this->request->data, array('validate' => false))) {
				$this->Session->setFlash(__('The subscription has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The subscription could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Subscription->findById($id);
		}
	}

	public function admin_delete($id = null) {
		$subscription = $this->Subscription->findById($id);
		if (!empty($subscription['Order'])) {
			$this->Session->setFlash(__('This subscription have associated orders, delete failed.'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Subscription->delete($id)) {
			$this->Session->setFlash(__('Subscription deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Subscription was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
