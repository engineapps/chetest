<?php

class PhotosController extends AppController {

	public $name = 'Photos';

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Upload->fileModel = 'Photo';
		$this->Upload->uploadDir = 'img/communities';
	}

	public function admin_index($community_id = null) {
		if ($community_id) {
			$this->paginate = array('conditions' => array('Photo.community_id' => $community_id));
		}

		$this->Photo->recursive = 0;
		$this->set('photos', $this->paginate());
		$this->set(compact('community_id'));
	}

	public function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid photo'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('photo', $this->Photo->findById($id));
	}

	public function admin_add($community_id = null) {
		if (!$this->request->is('get')) {
			$this->request->data['Photo']['name'] = $this->Upload->upload('name');

			$this->Photo->create();
			if ($this->Photo->save($this->request->data)) {
				$this->Session->setFlash('Photo has been saved.', 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Photo']['community_id']));
			} else {
				$this->Session->setFlash(__('The photo could not be saved. Please, try again.'));
			}
		}
		if ($community_id) {
			$this->request->data['Photo']['community_id'] = $community_id;
		}

		$communities = $this->Photo->Community->find('list');
		$types = $this->Photo->getTypes();
		$this->set(compact('communities', 'types'));
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid photo'));
			$this->redirect(array('action' => 'index'));
		}
		$photo = $this->Photo->findById($id);
		if (!$this->request->is('get')) {
			if (false == ($this->request->data['Photo']['name'] = $this->Upload->upload('name', $photo['Photo']['name']))) {
				unset($this->request->data['Photo']['name']);
			}

			if ($this->Photo->save($this->request->data)) {
				$this->Session->setFlash(__('The photo has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Photo']['community_id']));
			} else {
				$this->Session->setFlash(__('The photo could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Photo->findById($id);
		}
		$communities = $this->Photo->Community->find('list');
		$types = $this->Photo->getTypes();
		$this->set(compact('communities', 'types'));
	}

	public function admin_moveup($id, $delta = null) {
		$photo = $this->Photo->findById($id);
		if (empty($photo)) {
			$this->Session->setFlash(__('Invalid photo'));
			$this->redirect(array('action' => 'index'));
		}

		$prev = $this->Photo->find('all', array('conditions' => array('Photo.parent_id' => $photo['Photo']['parent_id'], 'Photo.lft <' => $photo['Photo']['lft']), 'order' => 'Photo.lft DESC'));
		if (!empty($prev) && $delta <= 1) {
			$delta = 1;
			foreach ($prev as $item) {
				if ($item['Community']['id'] == $photo['Community']['id']) {
					break;
				} else {
					$delta++;
				}
			}
		}

		$this->Photo->id = $photo['Photo']['id'];

		if ($delta > 0) {
			$this->Photo->moveUp($this->Photo->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide a number of positions the photo should be moved up.'));
		}

		$this->redirect($this->referer());
	}

	public function admin_movedown($id, $delta = null) {
		$photo = $this->Photo->findById($id);
		if (empty($photo)) {
			$this->Session->setFlash(__('Invalid photo'));
			$this->redirect(array('action' => 'index'), null, true);
		}

		$next = $this->Photo->find('all', array('conditions' => array('Photo.parent_id' => $photo['Photo']['parent_id'], 'Photo.lft >' => $photo['Photo']['lft'])));
		if (!empty($next) && $delta <= 1) {
			$delta = 1;
			foreach ($next as $item) {
				if ($item['Community']['id'] == $photo['Community']['id']) {
					break;
				} else {
					$delta++;
				}
			}
		}

		$this->Photo->id = $photo['Photo']['id'];

		if ($delta > 0) {
			$this->Photo->moveDown($this->Photo->id, abs($delta));
		} else {
			$this->Session->setFlash(__('Please provide the number of positions the field should be moved down.'));
		}

		$this->redirect($this->referer());
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for photo'));
			$this->redirect(array('action' => 'index'));
		}
		$photo = $this->Photo->findById($id);

		if ($this->Photo->delete($id)) {
			$this->Upload->remove($photo['Photo']['name']);
			$this->Session->setFlash('Photo deleted', 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index', $photo['Photo']['community_id']));
		}
		$this->Session->setFlash(__('Photo was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
