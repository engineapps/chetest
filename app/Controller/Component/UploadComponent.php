<?php

class UploadComponent extends Component {

	public $uploadDir = 'img';
	public $fileModel = 'Image';
	public $allowedTypes = array(
		'image/jpeg',
		'image/gif',
		'image/png',
		'image/pjpeg',
		'image/x-png',
		'application/x-shockwave-flash',
		'application/octet-stream',
		'application/pdf'
	);
	public $requestData;

	public function initialize(Controller $controller) {
		$this->requestData = $controller->request->data;
	}

	public function reinitialize($requestData) {
		$this->requestData = $requestData;
	}

	public function remove($name = null) {
		if (!$name) {
			return false;
		}

		// Remove original file
		$up_dir = WWW_ROOT . $this->uploadDir;
		$target_path = $up_dir . DS . $name;
		@unlink($target_path);

		// Remove thumbnails
		$thumb_dir = WWW_ROOT . 'thumbs' . DS . $this->uploadDir;
		$mask = '*____' . $name;
		if (is_dir($thumb_dir)) {
			chdir($thumb_dir);
			foreach (glob($mask) as $filename) {
				@unlink($thumb_dir . DS . $filename);
			}
			chdir(WWW_ROOT);
		}

		return true;
	}

	public function upload($file = 'image', $oldfile = null) {
		$uploadedFile = '';

		if ($this->__validFile($file)) {
			if (!empty($oldfile)) {
				$this->remove($oldfile);
			}

			$i = 1;
			$ext = $this->__ext($this->requestData[$this->fileModel][$file]['name']);
			$name = Inflector::slug(substr($this->requestData[$this->fileModel][$file]['name'], 0, -strlen($ext)), '-');
			$temp_path = WWW_ROOT . $this->uploadDir . DS . $name;
			$target_path = $temp_path . '.' . $ext;
			while (file_exists($target_path)) {
				$target_path = $temp_path . '-' . $i . '.' . $ext;
				$i++;
			}

			if (move_uploaded_file($this->requestData[$this->fileModel][$file]['tmp_name'], $target_path)) {
				$uploadedFile = basename($target_path);
			} else {
				throw new CakeException(__('Unable to save temp file to file system.'));
			}
		}

		return $uploadedFile;
	}

	private function __ext($file) {
		return end(explode('.', $file));
	}

	private function __validFile($file) {
		if ($this->requestData[$this->fileModel][$file] && $this->requestData[$this->fileModel][$file]['error'] != UPLOAD_ERR_OK) {
			return false;
		}

		if (array_search($this->requestData[$this->fileModel][$file]['type'], $this->allowedTypes) === false) {
			return false;
		}

		return true;
	}

}