<?php

class HomesController extends AppController {

	public $name = 'Homes';

	public function beforeFilter() {
		parent::beforeFilter();
		
		$this->Auth->allow('view');
	}
	
	public function view($slug, $image_id) {
		$home = $this->Home->find('first', array('conditions' => array('Home.slug' => $slug), 'contain' => array('Community', 'Style', 'Attachment', 'Image' => array('Elevation'))));
		$image = $this->Home->Image->findById($image_id);
		if (!$home) {
			$this->Session->setFlash(__('Invalid home'));
			$this->redirect(array('controller' => 'homes', 'action' => 'index'));
		} else {
			$title_for_layout = __('Model: %s %s - %s', $home['Home']['name'], $image['Elevation']['abbrev'], $home['Community']['name']);
			$page['Page'] = $image['Image'];
		}

		$this->breadcrumbs[1] = array(Inflector::humanize($home['Community']['name']), array('controller' => 'community', 'action' => $home['Community']['slug']));
		$this->breadcrumbs[2][0] = $home['Home']['name'];

		if ($this->request->isAjax()) {
			$this->layout = 'ajax';
		}
		$this->set('slug', 'model');
		$this->set(compact('title_for_layout', 'home', 'page', 'image', 'breadcrumbs'));
	}
	
	
	public function admin_index($community_id = null) {
		$conditions = !empty($community_id) ? array('Community.id' => $community_id) : null;

		$this->set('homes', $this->paginate($conditions));
	}

	public function admin_view($id = null) {
		$home = $this->Home->find('first', array('conditions' => array('Home.id' => $id), 'contain' => array('Community', 'Style', 'Image' => array('Elevation'))));
		
		if (!$home) {
			$this->Session->setFlash(__('Invalid home'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set(compact('home'));
	}

	public function admin_add($community_id = null) {
		if (!$this->request->is('get')) {
			$this->Home->create();
			if ($this->Home->save($this->request->data)) {
				$this->Session->setFlash(__('The home has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Home']['community_id']));
			} else {
				$this->Session->setFlash(__('The home could not be saved. Please, try again.'));
			}
		}
		if (!empty($community_id)) {
			$this->request->data['Home']['community_id'] = $community_id;
		}
		$communities = $this->Home->Community->find('list');
		$styles = $this->Home->Style->find('list');
		$this->set(compact('communities', 'styles'));
	}

	public function admin_edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid home'));
			$this->redirect(array('action' => 'index'));
		}
		$home = $this->Home->findById($id);
		if (!$this->request->is('get')) {
			if ($this->Home->save($this->request->data)) {
				$this->Session->setFlash(__('The home has been saved'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index', $this->request->data['Home']['community_id']));
			} else {
				$this->Session->setFlash(__('The home could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $home;
		}
		$communities = $this->Home->Community->find('list');
		$styles = $this->Home->Style->find('list');
		$this->set(compact('communities', 'styles'));
	}

	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for home'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Home->delete($id)) {
			$this->Session->setFlash(__('Home deleted'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index', $home['Home']['community_id']));
		}
		$this->Session->setFlash(__('Home was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}