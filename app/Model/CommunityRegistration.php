<?php

class CommunityRegistration extends AppModel {

	var $name = 'CommunityRegistration';
	var $useTable = 'communities_registrations';
	var $belongsTo = array('Registration', 'Community');
	var $actsAs = array('Containable');
	
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

	}

}