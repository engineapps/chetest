<?php

class Photo extends AppModel {

	public $name = 'Photo';
	public $belongsTo = array('Community');
	public $actsAs = array('Containable', 'Tree');

	const PHOTO_GALLERY = 'gallery';
	
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->order = $this->alias . '.lft ASC';
		$this->virtualFields = array(
			'is_gallery' => 'IF(' . $this->alias . '.type = "' . self::PHOTO_GALLERY . '", 1, 0)'
		);
		
		$this->validate = array(
			'title' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			),
			'name' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			)
		);
	}
	
	public function getTypes () {
		return array(
			self::PHOTO_GALLERY => __('Gallery')
		);
	}
}