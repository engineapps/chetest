<?php

class Elevation extends AppModel {

	public $name = 'Elevation';
	public $hasMany = array('Image');

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->validate = array(
			'name' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'abbrev' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			)
		);
	}

}