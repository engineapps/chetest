<?php

class Home extends AppModel {

	public $name = 'Home';
	public $belongsTo = array('Community', 'Style');
	public $hasMany = array('Image', 'Attachment');
	public $actsAs = array('Containable');
	
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->virtualFields = array(
			'sort_home_first' => 'SUBSTRING_INDEX(Home.name, " ", 1)',
			'sort_home_last' => 'CAST(REPLACE(SUBSTRING_INDEX(Home.name, " ", -1), "A", "0") AS SIGNED)'
		);
		
		$this->validate = array(
			'name' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'title' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'slug' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				),
				'unique' => array(
					'rule' => 'isUnique',
					'message' => $this->errorMessages['unique']
				)
			),
			'bedrooms' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'bathrooms' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'community_id' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'style_id' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
		);
	}
}