<?php

App::uses('AuthComponent', 'Controller/Component');

class User extends AppModel {

	public $name = 'User';
	public $displayField = 'full_name';

	const USER_PUBLIC = 10;
	const USER_SPECIAL = 20;
	const USER_ADMIN = 30;

	static $userLevels = array(
		self::USER_PUBLIC => 'Public',
		self::USER_SPECIAL => 'Special',
		self::USER_ADMIN => 'Admin'
	);

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->virtualFields = array(
			'full_name' => 'CONCAT(' . $this->alias . '.first_name, " ", ' . $this->alias . '.last_name)',
			'is_admin' => 'IF(' . $this->alias . '.level = ' . self::USER_ADMIN . ', 1, 0)',
			'is_special' => 'IF(' . $this->alias . '.level = ' . self::USER_SPECIAL . ', 1, 0)',
			'is_public' => 'IF(' . $this->alias . '.level = ' . self::USER_PUBLIC . ', 1, 0)'
		);

		$this->validate = array(
			'first_name' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'last_name' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'email' => array(
				'email' => array(
					'rule' => 'email',
					'message' => $this->errorMessages['email']
				),
				'unique' => array(
					'rule' => 'isUnique',
					'message' => __('This email is already registered with us.')
				)
			),
			'password' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'old_password' => array(
				'required' => array(
					'rule' => 'oldPassword',
					'message' => __('Old password is incorrect')
				)
			),
			'new_password' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'confirm_password' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				),
				'match' => array(
					'rule' => 'confirmPassword',
					'message' => __('Both passwords does not match')
				)
			),
			'level' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'phone' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				),
				'regexPhone' => array(
					'rule' => '/^[0-9]{10}$/',
					'message' => __('Phone number must be 10 digits only')
				)
			),
			'type' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'address' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'city' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'postal' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				),
				'postal' => array(
					'rule' => '/^[A-Za-z]{1}\d{1}[A-Za-z]{1}\d{1}[A-Za-z]{1}\d{1}$/i',
					'message' => $this->errorMessages['postal']
				)
			)
		);
	}

	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		return true;
	}

	public function confirmPassword() {
		if (empty($this->data[$this->alias]['new_password']) || $this->data[$this->alias]['new_password'] != $this->data[$this->alias]['confirm_password']) {
			return false;
		}
		return true;
	}

	public function oldPassword() {
		if (empty($this->data[$this->alias]['id'])) {
			return false;
		}
		return $this->find('count', array('conditions' => array($this->alias . '.password' => $this->data[$this->alias]['old_password'], $this->alias . '.id' => $this->data[$this->alias]['id'])));
	}

	public function isAdmin($level = 0) {
		return (bool) $level == self::USER_ADMIN;
	}

	public function randomString($length = 9, $strength = 3) {
		$vowels = 'aeuy';
		$consonants = 'bdghjmnpqrstvz';
		if ($strength >= 1) {
			$consonants .= 'BDGHJLMNPQRSTVWXZ';
		}
		if ($strength >= 2) {
			$vowels .= "AEUY";
		}
		if ($strength >= 3) {
			$consonants .= '23456789';
		}
		if ($strength >= 4) {
			$consonants .= '@#$%';
		}

		$password = '';
		$alt = time() % 2;
		for ($i = 0; $i < $length; $i++) {
			if ($alt == 1) {
				$password .= $consonants[(rand() % strlen($consonants))];
				$alt = 0;
			} else {
				$password .= $vowels[(rand() % strlen($vowels))];
				$alt = 1;
			}
		}
		return $password;
	}
	
}