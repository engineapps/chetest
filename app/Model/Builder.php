<?php

class Builder extends AppModel {

	public $name = 'Builder';

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->validate = array(
			'name' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			)
		);
	}

}
