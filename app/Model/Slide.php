<?php

class Slide extends AppModel {

	public $name = 'Slide';
	public $belongsTo = array('Page');
	public $order = 'Slide.lft ASC';
	public $actsAs = array('Tree');

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->validate = array(
			'title' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'image' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			)
		);
	}

	public function getSlides($page_id = null) {
		$results = $this->find('all', array('conditions' => array('Slide.page_id' => $page_id, 'Slide.is_active' => '1')));

		if (!empty($results)) {
			foreach ($results as $i => $result) {
				$results[$i]['Slide']['description'] = __replaceVars($result['Slide']['description']);
			}
		}

		return $results;
	}

}
