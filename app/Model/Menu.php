<?php

class Menu extends AppModel {

	public $name = 'Menu';
	public $hasMany = array('Link');
	public $cacheKey = 'nav';

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->validate = array(
			'name' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'slug' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				),
				'unique' => array(
					'rule' => 'isUnique',
					'message' => $this->errorMessages['unique']
				)
			)
		);
	}
	
	public function afterSave($created) {
		parent::afterSave($created);
		
		Cache::delete($this->cacheKey);
	}

	/**
	 * Generate all menus in both format, tree data structure and compiled html markup
	 * e.g. $menus  = array('data' => array('', ...), 'markup' => array('', ...));
	 * 
	 * @return array
	 */
	public function getMenus() {
		if (Cache::read($this->cacheKey)) {
			return Cache::read($this->cacheKey);
		} else {
			$menus = $this->find('list', array('fields' => array('Menu.id', 'Menu.slug')));

			$output = array();
			$links = $this->Link->children();
			foreach ($links as $link) {
				$link['Link']['url'] = Router::url($link['Link']['url']);
				$i = !empty($link['Link']['parent_id']) ? $link['Link']['parent_id'] : 0;
				$output['data'][$menus[$link['Link']['menu_id']]][$i][$link['Link']['id']] = $link['Link'];
			}

			foreach ($output['data'] as $key => $item) {
				if (is_array($item) && array_key_exists(0, $item)) {
					$output['markup'][$key] = $this->__getNodes($item[0], $item, false);
				}
			}
			
			Cache::write($this->cacheKey, $output);

			return $output;
		}
	}

	/**
	 * Compile tree data sturcture to html markup with nested `ul` tags
	 * 
	 * @param type $nodes
	 * @param type $mainList
	 * @return string
	 */
	private function __getNodes($nodes, $mainList) {
		$result = ($nodes === $mainList[0]) ? '' : '<ul class="dropdown-menu">';
		foreach ($nodes as $nodeKey => $name) {
			$result .= $this->__getNode($nodeKey, $name, $mainList);
		}
		$result .= '</ul>';
		return $result;
	}

	/**
	 * Compile tree data sturcture to html markup with nested `li` tags
	 *
	 * @param type $key
	 * @param type $value
	 * @param type $mainList
	 * @return string
	 */
	private function __getNode($key, $value, $mainList) {
		$dropdown = (array_key_exists($key, $mainList)) ? true : false; 
		$result = '<li class="' . ($dropdown ? 'dropdown' : '') . '">';
		$result .= '<a ' . ($dropdown ? 'class="dropdown-toggle" data-toggle="dropdown"' : '') . ' href="' . $value['url'] . '">' . '<span class="name">' . $value['name'] . '' . ($dropdown ? '<b class="caret"></b>' : '') . '</span><span class="subtext">' . $value['subtext'] . '</span></a>';
		if ($dropdown) {
			$result .= $this->__getNodes($mainList[$key], $mainList);
		}
		$result .= '</li>';
		return $result;
	}

}