<?php

class Subscription extends AppModel {

	public $name = 'Subscription';
	public $displayField = 'full_name';
	public $hasMany = array('LogSubscription');

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->virtualFields = array(
			'full_name' => 'CONCAT(' . $this->alias . '.first_name, " ", ' . $this->alias . '.last_name)'
		);

		$this->validate = array(
			'first_name' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'last_name' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'email' => array(
				'email' => array(
					'rule' => 'email',
					'message' => $this->errorMessages['email']
				),
				'unique' => array(
					'rule' => 'isUnique',
					'message' => __('You are already subscribed')
				)
			),
			'is_subscribed' => array(
				'nonzero' => array(
					'rule' => array('comparison', '>', 0),
					'message' => __('You must select this checkbox to continue')
				),
			),
		);
	}
	
	public function save($data = null, $validate = true, $fieldList = array()) {
		if (!empty($data[$this->alias]['id']) || !empty($data['id'])) {
			$id = !empty($data[$this->alias]['id']) ? $data[$this->alias]['id'] : $data['id'];
			$this->recursive = -1;
			$record = $this->findById($id);
			if (!empty($record[$this->alias]['id'])) {
				$record[$this->alias]['subscription_id'] = $record[$this->alias]['id'];
				unset($record[$this->alias]['id']);
				$this->LogSubscription->create();
				$this->LogSubscription->save($record[$this->alias]);
			}
		}

		return parent::save($data, $validate, $fieldList);
	}
	
}