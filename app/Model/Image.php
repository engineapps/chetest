<?php

class Image extends AppModel {

	public $name = 'Image';
	public $belongsTo = array('Elevation', 'Home' => array(
		'order' => 'Home.sort_home_first ASC, Home.sort_home_last ASC'
	));
	public $actsAs = array('Containable');

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->virtualFields = array(
			'name_gdata' => 'SUBSTRING_INDEX(' . $this->alias . '.name, ".jpg", 1)',
		);
		$this->validate = array(
			'name' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			),
			'size' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			),
			'price' => array(
				'rule' => '/^[0-9]{1,14}\.?[0-9]{0,2}$/i',
				'message' => $this->errorMessages['price']
			),
		);
	}

}
