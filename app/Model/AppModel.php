<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	public $errorMessages;
	public $appConfig;
	public $searchModels;

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->errorMessages = array(
			'empty' => __('This field can\'t be blank'),
			'price' => __('Please enter valid price e.g. 19.45'),
			'numeric' => __('Please enter valid numeric value'),
			'unique' => __('This value already exists, try again.'),
			'date' => __('Enter a valid date in YYYY-MM-DD format'),
			'time' => __('Enter a valid time in HH:MM (am or pm) format'),
			'email' => __('Enter a valid email address'),
			'phone' => __('Enter a valid phone number'),
			'postal' => __('Enter a valid postal code e.g. L6T2M3'),
		);

		$this->searchModels = array(
			'User' => array(
				'full_name',
				'email',
				'phone'
			),
			'Category' => array(
				'name',
				'slug'
			),
			'Page' => array(
				'name',
				'slug',
				'title',
			),
			'Link' => array(
				'name'
			),
			'Menu' => array(
				'name'
			)
		);

		$this->appConfig = Configure::read('App');
	}
	
	public function getParentNodes($id) {
		$items = array();
		do {
			$item = $this->getParentNode($id);
			if (!empty($item)) {
				$items[$item[$this->alias]['id']] = $item[$this->alias][$this->displayField];
			}
			$id = $item[$this->alias]['parent_id'];
		} while(!empty($item));
		
		return $items;
	}

	public function beforeValidate($options = array()) {
		parent::beforeValidate($options);

		if (!empty($this->data[$this->alias]['slug'])) {
			$this->data[$this->alias]['slug'] = Inflector::slug(strtolower($this->data[$this->alias]['slug']), '-');
		}
	}

	public function getSearchModels() {
		return $this->searchModels;
	}
}
