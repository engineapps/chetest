<?php

class Community extends AppModel {

	public $name = 'Community';
	public $hasMany = array(
			'Photo',
			'Home' => array(
				'conditions' => array('Home.is_active' => '1')
			), 'CommunityRegistration');
	public $belongsTo = array('Office');
	public $displayField = 'name';
	public $actsAs = array('Tree', 'Containable');

	const TYPE_RESIDENTIAL = 'residential';
	const TYPE_COMMERCIAL = 'commercial';

	const GRID_DEFAULT = 'default';
	const GRID_DOUBLEWIDTH = 'doubleWidth';
	const GRID_DOUBLEHEIGHT = 'doubleHeight';

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->virtualFields = array(
			'is_residential' => 'IF(' . $this->alias . '.type = "' . self::TYPE_RESIDENTIAL . '", 1, 0)',
			'is_commercial' => 'IF(' . $this->alias . '.type = "' . self::TYPE_COMMERCIAL . '", 1, 0)'
		);

		$this->order = $this->alias . '.lft ASC';
		$this->validate = array(
			'name' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			),
			'slug' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				),
				'isUnique' => array(
					'rule' => 'isUnique',
					'message' => $this->errorMessages['unique']
				)
			)
		);
	}

	public function getTypes() {
		return array(
			self::TYPE_RESIDENTIAL => __('Residential'),
			self::TYPE_COMMERCIAL => __('Commercial')
		);
	}

	public function getGrids() {
		return array(
			self::GRID_DEFAULT => __('Default'),
			self::GRID_DOUBLEWIDTH => __('Double Width'),
			self::GRID_DOUBLEHEIGHT => __('Double Height')
		);
	}

	public function afterFind($results, $primary = false) {
		parent::afterFind($results, $primary);

		foreach ($results as &$result) {
			if (isset($result[$this->alias]['description'])) {
				$result[$this->alias]['formatted'] = __replaceVars($result[$this->alias]['description']);
			} else if (isset($result['contents'])) {
				$result['formatted'] = __replaceVars($result['description']);
			}
		}

		return $results;
	}
}