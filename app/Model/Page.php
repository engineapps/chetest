<?php

class Page extends AppModel {

	public $name = 'Page';
	public $displayField = 'name';
	public $hasMany = array('Slide');
	public $order = 'Page.modified DESC';

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->validate = array(
			'name' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'slug' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				),
				'isUnique' => array(
					'rule' => 'isUnique',
					'message' => $this->errorMessages['unique']
				)
			)
		);
	}
	
	public function getAnalytics() {
		$optionsKey = 'gaGraphData';
        $graphData = array();
		if (Cache::read($optionsKey)) {
			return Cache::read($optionsKey);
		} else if (!empty($this->appConfig['GoogleAnalytics']['profileId'])) {
			App::import('Vendor', 'gapi', array('file' => 'gapi' . DS . 'gapi.class.php'));
			$ga = new gapi($this->appConfig['GoogleAnalytics']['gapiEmail'], $this->appConfig['GoogleAnalytics']['gapiPassword']);
			$graphData = array('Pageviews' => array('["Date", "Pageviews"]'), 'Visits' => array('["Date", "Visits", "New Visits"]'), 'Percentages' => array('["Statistic", "Information"]'));

			$ga->requestReportData($this->appConfig['GoogleAnalytics']['profileId'], array('date'), array('pageviews', 'visits', 'newVisits', 'percentNewVisits', 'visitBounceRate'), array('date'), null, date('Y-m-d', strtotime('-10 days')), date('Y-m-d'));
			$gaResults = $ga->getResults();
			for ($i = 0, $last = count($gaResults); $i < $last; $i++) {
				$metrics = $gaResults[$i]->getMetrics();
				$dimensions = $gaResults[$i]->getDimesions();
				$graphData['Pageviews'][] = '["' . date('j M', strtotime($dimensions['date'])) . '", ' . $metrics['pageviews'] . ']';
				$graphData['Visits'][] = '["' . date('j M', strtotime($dimensions['date'])) . '", ' . $metrics['visits'] . ', ' . $metrics['newVisits'] . ']';
				if ($i == ($last - 1)) {
					// Present Results
					$graphData['Percentages'][] = '["' . __('New Visitors') . '", ' . round($metrics['percentNewVisits']) . ']';
					$graphData['Percentages'][] = '["' . __('Returning Visitors') . '", ' . round(100 - $metrics['percentNewVisits']) . ']';
					$graphData['VisitBounceRate'] = round($metrics['visitBounceRate']);
				}
			}

			Cache::write($optionsKey, $graphData);
		}
		return $graphData;
	}

	public function readDirectory($dir) {
		$results = array();

		$mimes = array('image/png', 'image/jpg', 'image/gif', 'image/jpeg', 'image/pjpeg');
		$cdir = scandir($dir);
		foreach ($cdir as $key => $value) {
			if (!in_array($value, array(".", ".."))) {
				if (is_dir($dir . DS . $value)) {
					$results = array_merge($results, $this->readDirectory($dir . DS . $value));
				} else {
					$image = @getimagesize($dir . DS . $value);
					if (!empty($image['mime']) && array_search($image['mime'], $mimes) !== false) {
						$results[] = array('thumb' => Router::url(DS . 'thumbs' . DS . $dir . DS . '100____0____' . $value), 'image' => Router::url(DS . $dir . DS . $value), 'title' => $value, 'folder' => Router::url(DS . $dir));
					}
				}
			}
		}

		return $results;
	}

}