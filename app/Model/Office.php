<?php

class Office extends AppModel {

	public $name = 'Office';
	public $hasMany = 'Community';
	public $displayField = 'name';
	public $actsAs = array('Tree');

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->order = $this->alias . '.lft ASC';
		
		$this->validate = array(
			'name' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			),
			'phone' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			),
			'address' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			),
			'city' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			),
			'province' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			),
			'lat' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			),
			'lng' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			)
		);
	}

}
