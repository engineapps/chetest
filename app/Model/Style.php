<?php

class Style extends AppModel {

	public $name = 'Style';
	public $actsAs = array('Tree', 'Containable');
	public $hasMany = array('Home');
	public $order = 'Style.lft ASC';

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->validate = array(
			'name' => array(
				'rule' => 'notEmpty',
				'message' => $this->errorMessages['empty']
			),
		);
	}
}