<?php

class Registration extends AppModel {

	public $name = 'Registration';
	public $hasMany = array('CommunityRegistration');
	public $hasAndBelongsToMany = array('Style', 'Community');

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->virtualFields = array(
			'full_name' => 'CONCAT(' . $this->alias . '.first_name, " ", ' . $this->alias . '.last_name)',
		);
		
		$this->validate = array(
			'first_name' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'last_name' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'email' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				),
				'email' => array(
					'rule' => 'email',
					'message' => $this->errorMessages['email']
				)
			),
			'phone' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'address' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'city' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'province' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'postal' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			)
		);
	}

	public function getheardFrom() {
		return array(
			'google' => __('Google'),
			'other-search-engine' => __('Other search engine'),
			'facebook-twitter' => __('Facebook/Twitter'),
			'signage-billboard' => __('Signage/Billboard'),
			'homes-magazine' => __('Homes Magazine'),
			'referred-by-friend' => __('Referred by Friend'),
			'other' => __('Others'),
		);
	}
	
	public function getAgeRanges() {
		return array(
			'25-35' => '25 to 35 years old',
			'35-45' => '35 to 45 years old',
			'45+' => '45+ years old'
		);
	}

	public function getProvinces() {
		return array(
			'ON' => __('Ontario'),
			'AB' => __('Alberta'),
			'BC' => __('British Columbia'),
			'MB' => __('Manitoba'),
			'NL' => __('Newfoundland and Labrador'),
			'NS' => __('Nova Scotia'),
			'NB' => __('New Brunswick'),
			'NT' => __('Northwest Territories'),
			'NU' => __('Nunavut'),
			'PE' => __('Prince Edward Island'),
			'QC' => __('Quebec'),
			'SK' => __('Saskatchewan'),
			'YT' => __('Yukon Territory')
		);
	}

	public function generatePassword($length = 8) {
		$password = "";
		$i = 0;
		$possible = "23456789bcdfghjkmnpqrstvwxyz";

		while ($i < $length) {
			$char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);

			if (!strstr($password, $char)) {
				$password .= $char;
				$i++;
			}
		}
		return $password;
	}

}