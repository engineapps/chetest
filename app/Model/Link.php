<?php

class Link extends AppModel {

	public $name = 'Link';
	public $belongsTo = array('Menu');
	public $actsAs = array('Tree');

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);

		$this->order = $this->alias . '.lft';
		$this->validate = array(
			'name' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			),
			'url' => array(
				'required' => array(
					'rule' => 'notEmpty',
					'message' => $this->errorMessages['empty']
				)
			)
		);
	}

	public function beforeSave($options = array()) {
		if(!empty($this->data['Link']['url'])) {
			$this->data['Link']['url'] = trim($this->data['Link']['url']);
		}
		
		return true;
	}
	
	public function afterSave($created) {
		parent::afterSave($created);
		
		Cache::delete($this->Menu->cacheKey);
	}
}